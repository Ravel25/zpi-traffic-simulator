﻿using UnityEngine;
using System.Collections;

public class SingleTurnRight : Road
{
    public Waypoint waypointR,
         waypointL1,
         waypointL2,
         waypointL3;



    override public void UpdateGrid()
    {
        int[] coordinates = GridManager.GetGridCoordinates(this.transform.position);
        GridX = coordinates[0];
        GridY = coordinates[1];

        if (GridManager.IsSpotVacant(this))
        {
            GridManager.PlaceObjectOnTheGrid(this);
            waypointR.UpdateGridCoordinates();
            waypointL1.UpdateGridCoordinates();
            waypointL2.UpdateGridCoordinates();
            waypointL3.UpdateGridCoordinates();

            int yWaypointOffset = GridHeight / 2;


            IGridObject east = GridManager.GetGridObject(GridX - GridHeight, GridY);
			if (east != null && east.GridX == GridX  - GridHeight && east.GridY == GridY && (east is StraightRoadRotatedEastward || east is SingleTurnLeft1 || east is SingleTurnLeft ))
            {
                Waypoint rEast = GridManager.GetWaypoint(waypointL1.gridX - 1, waypointL1.gridY);
                if (rEast != null)
                    rEast.AddWaypoint(waypointL1, 1);

                Waypoint lEast = GridManager.GetWaypoint(waypointL2.gridX - 1, waypointL2.gridY);
                if (lEast != null)
                    waypointL2.AddWaypoint(lEast, 1);
            }

            IGridObject north = GridManager.GetGridObject(GridX, GridY - GridHeight);
			if (north != null && north.GridX == GridX && north.GridY == GridY - GridHeight && (north is StraightRoadRotatedNorthward || north is SingleTurnLeft || north is SingleTurnRight1))
            {
                Waypoint rNorth = GridManager.GetWaypoint(waypointL1.gridX, waypointL1.gridY - yWaypointOffset);
                if (rNorth != null)
                    waypointL1.AddWaypoint(rNorth, 1);

                Waypoint lNorth = GridManager.GetWaypoint(waypointR.gridX, waypointR.gridY - yWaypointOffset);
                if (lNorth != null)
                    lNorth.AddWaypoint(waypointR, 1);
            }
        }
    }
	public override void DrawGizmos(Color color)
	{
		waypointR.DrawGizmos(color);
		waypointL1.DrawGizmos(color);
		waypointL2.DrawGizmos(color);
		waypointL3.DrawGizmos(color);
	}
}
