﻿using UnityEngine;
using System.Collections;

public class DoubleTurnLeft1 : Road {
	
	public Waypoint waypoint11, waypoint12, waypoint13, waypoint14,
	waypoint21, waypoint22, waypoint23, waypoint24,
	waypoint31, waypoint32, waypoint33, waypoint34,
	waypoint41, waypoint42, waypoint43, waypoint44;

	public override void UpdateGrid()
	{
		int[] coordinates = GridManager.GetGridCoordinates (this.transform.position);
		GridX = coordinates [0];
		GridY = coordinates [1];
		
		if (GridManager.IsSpotVacant (this)) {
			GridManager.PlaceObjectOnTheGrid (this);
			waypoint11.UpdateGridCoordinates (); 
			waypoint12.UpdateGridCoordinates (); 
			waypoint13.UpdateGridCoordinates (); 
			waypoint14.UpdateGridCoordinates ();
			waypoint21.UpdateGridCoordinates (); 
			waypoint22.UpdateGridCoordinates (); 
			waypoint23.UpdateGridCoordinates (); 
			waypoint24.UpdateGridCoordinates ();
			waypoint31.UpdateGridCoordinates (); 
			waypoint32.UpdateGridCoordinates ();
			waypoint33.UpdateGridCoordinates (); 
			waypoint34.UpdateGridCoordinates ();
			waypoint41.UpdateGridCoordinates (); 
			waypoint42.UpdateGridCoordinates (); 
			waypoint43.UpdateGridCoordinates (); 
			waypoint44.UpdateGridCoordinates ();
			
			
			
			IGridObject east = GridManager.GetGridObject (GridX - 2, GridY);
			if (east != null && east.GridX == GridX - 2 && east.GridY == GridY && (east is StraightDoubleRoadRotatedEastward)) {
				
				Waypoint rlEast = GridManager.GetWaypoint (waypoint21.gridX - 1, waypoint21.gridY);
				if (rlEast != null)
                    waypoint21.AddWaypoint(rlEast, 1);
				
				Waypoint lrEast = GridManager.GetWaypoint (waypoint41.gridX - 1, waypoint41.gridY);
				if (lrEast != null)
                    lrEast.AddWaypoint(waypoint41, 1);
				
				Waypoint rrEast = GridManager.GetWaypoint (waypoint11.gridX - 1, waypoint11.gridY);
				if (rrEast != null)
                    waypoint11.AddWaypoint(rrEast, 1);
				
				Waypoint llEast = GridManager.GetWaypoint (waypoint31.gridX - 1, waypoint31.gridY);
				if (llEast != null)
                    llEast.AddWaypoint(waypoint31, 1);
			}

			IGridObject eastTurn = GridManager.GetGridObject (GridX - 4, GridY);
			if (eastTurn != null && eastTurn.GridX == GridX - 4 && eastTurn.GridY == GridY && (eastTurn is DoubleTurnRight1 || eastTurn is DoubleTurnRight)) {
				
				Waypoint rlEast = GridManager.GetWaypoint (waypoint21.gridX - 1, waypoint21.gridY);
				if (rlEast != null)
                    waypoint21.AddWaypoint(rlEast, 1);
				
				Waypoint lrEast = GridManager.GetWaypoint (waypoint41.gridX - 1, waypoint41.gridY);
				if (lrEast != null)
                    lrEast.AddWaypoint(waypoint41, 1);
				
				Waypoint rrEast = GridManager.GetWaypoint (waypoint11.gridX - 1, waypoint11.gridY);
				if (rrEast != null)
                    waypoint11.AddWaypoint(rrEast, 1);
				
				Waypoint llEast = GridManager.GetWaypoint (waypoint31.gridX - 1, waypoint31.gridY);
				if (llEast != null)
                    llEast.AddWaypoint(waypoint31, 1);
			}

			
			IGridObject south = GridManager.GetGridObject (GridX, GridY + 4);
			if (south != null && south.GridX == GridX && south.GridY == GridY + 4 && (south is StraightDoubleRoadRotatedNorthward || south is DoubleTurnLeft || south is DoubleTurnRight)) 
			{
				Waypoint rlSouth = GridManager.GetWaypoint (waypoint11.gridX, waypoint11.gridY + 1);
				if (rlSouth != null)
                    rlSouth.AddWaypoint(waypoint11, 1);
				
				Waypoint lrSouth = GridManager.GetWaypoint (waypoint13.gridX, waypoint13.gridY + 1);
				if (lrSouth != null)
                    waypoint13.AddWaypoint(lrSouth, 1);
				
				Waypoint rrSouth = GridManager.GetWaypoint (waypoint12.gridX, waypoint12.gridY + 1);
				if (rrSouth != null)
                    rrSouth.AddWaypoint(waypoint12, 1);
				
				Waypoint llSouth = GridManager.GetWaypoint (waypoint14.gridX, waypoint14.gridY + 1);
				if (llSouth != null)
                    waypoint14.AddWaypoint(llSouth, 1);
			}

		}
	}
	
	public override void DrawGizmos(Color color)
	{
		waypoint11.DrawGizmos(color); 
		waypoint12.DrawGizmos(color);  
		waypoint13.DrawGizmos(color);  
		waypoint14.DrawGizmos(color); 
		waypoint21.DrawGizmos(color); 
		waypoint22.DrawGizmos(color);  
		waypoint23.DrawGizmos(color);  
		waypoint24.DrawGizmos(color); 
		waypoint31.DrawGizmos(color); 
		waypoint32.DrawGizmos(color); 
		waypoint33.DrawGizmos(color); 
		waypoint34.DrawGizmos(color); 
		waypoint41.DrawGizmos(color); 
		waypoint42.DrawGizmos(color); 
		waypoint43.DrawGizmos(color); 
		waypoint44.DrawGizmos(color); 
	}
}
