﻿using UnityEngine;
using System.Collections;

public class SingleTurnLeft : Road {

    public Waypoint waypointL,
        waypointR1,
        waypointR2,
        waypointR3;

    

    override public void UpdateGrid()
    {
        int[] coordinates = GridManager.GetGridCoordinates(this.transform.position);
        GridX = coordinates[0];
        GridY = coordinates[1];

        if (GridManager.IsSpotVacant(this))
        {
            GridManager.PlaceObjectOnTheGrid(this);
            waypointL.UpdateGridCoordinates();
            waypointR1.UpdateGridCoordinates();
            waypointR2.UpdateGridCoordinates();
            waypointR3.UpdateGridCoordinates();

            int yWaypointOffset = GridHeight / 2;


            IGridObject west = GridManager.GetGridObject(GridX + GridHeight, GridY);
			if (west != null && west.GridX == GridX + GridHeight&& west.GridY == GridY  && (west is StraightRoadRotatedEastward || west is SingleTurnRight || west is SingleTurnRight1))
            {
                Waypoint rWest = GridManager.GetWaypoint(waypointL.gridX + 1, waypointR3.gridY);
                if (rWest != null)
                    waypointL.AddWaypoint(rWest, 1);

                Waypoint lWest = GridManager.GetWaypoint(waypointR1.gridX + 1, waypointR1.gridY);
                if (lWest != null)
                    lWest.AddWaypoint(waypointR1, 1);
            }
            IGridObject south = GridManager.GetGridObject(GridX, GridY + GridHeight);
			if (south != null && south.GridX == GridX && south.GridY == GridY + GridHeight && (south is StraightRoadRotatedNorthward || south is SingleTurnLeft1 || south is SingleTurnRight))
            {
                Waypoint rSouth = GridManager.GetWaypoint(waypointR1.gridX, waypointR1.gridY + yWaypointOffset);
                if (rSouth != null)
                    waypointR1.AddWaypoint(rSouth, 1);

                Waypoint lSouth = GridManager.GetWaypoint(waypointR2.gridX, waypointR2.gridY + yWaypointOffset);
                if (lSouth != null)
                    lSouth.AddWaypoint(waypointR2, 1);
            }
        }
    }
	public override void DrawGizmos(Color color)
	{
		waypointL.DrawGizmos(color);
		waypointR1.DrawGizmos(color);
		waypointR2.DrawGizmos(color);
		waypointR3.DrawGizmos(color);
	}
}
