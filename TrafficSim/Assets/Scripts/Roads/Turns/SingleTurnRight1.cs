﻿using UnityEngine;
using System.Collections;

public class SingleTurnRight1 : Road 
{
    public Waypoint waypointR,
        waypointL1,
        waypointL2,
        waypointL3;


    override public void UpdateGrid()
    {
        int[] coordinates = GridManager.GetGridCoordinates(this.transform.position);
        GridX = coordinates[0];
        GridY = coordinates[1];

        if (GridManager.IsSpotVacant(this))
        {
            GridManager.PlaceObjectOnTheGrid(this);
            waypointR.UpdateGridCoordinates();
            waypointL1.UpdateGridCoordinates();
            waypointL2.UpdateGridCoordinates();
            waypointL3.UpdateGridCoordinates();

            int yWaypointOffset = GridHeight / 2;


            IGridObject east = GridManager.GetGridObject(GridX - GridHeight, GridY);
			if (east != null && east.GridX == GridX - GridHeight && east.GridY == GridY && (east is StraightRoadRotatedEastward || east is SingleTurnLeft1 || east is SingleTurnLeft))
            {
                Waypoint rEast = GridManager.GetWaypoint(waypointL2.gridX - 1, waypointL2.gridY);
                if (rEast != null)
                    waypointL2.AddWaypoint(rEast, 1);

                Waypoint lEast = GridManager.GetWaypoint(waypointL1.gridX - 1, waypointL1.gridY);
                if (lEast != null)
                    lEast.AddWaypoint(waypointL1, 1);
            }

            IGridObject south = GridManager.GetGridObject(GridX, GridY + GridHeight);
			if (south != null && south.GridX == GridX && south.GridY == GridY + GridHeight && (south is StraightRoadRotatedNorthward || south is SingleTurnLeft1 || south is SingleTurnRight))
            {
                Waypoint rSouth = GridManager.GetWaypoint(waypointL2.gridX, waypointL2.gridY + yWaypointOffset);
                if (rSouth != null)
                    rSouth.AddWaypoint(waypointL2, 1);

                Waypoint lSouth = GridManager.GetWaypoint(waypointL3.gridX, waypointL3.gridY + yWaypointOffset);
                if (lSouth != null)
                    waypointL3.AddWaypoint(lSouth, 1);
            }
        }
    }
	public override void DrawGizmos(Color color)
	{
		waypointR.DrawGizmos(color);
		waypointL1.DrawGizmos(color);
		waypointL2.DrawGizmos(color);
		waypointL3.DrawGizmos(color);
	}
}
