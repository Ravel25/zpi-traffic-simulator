﻿using UnityEngine;
using System.Collections;

public class SingleTurnLeft1 : Road {

    public Waypoint waypointL,
        waypointR1,
        waypointR2,
        waypointR3;

   
    override public void UpdateGrid()
    {
        int[] coordinates = GridManager.GetGridCoordinates(this.transform.position);
        GridX = coordinates[0];
        GridY = coordinates[1];

        if (GridManager.IsSpotVacant(this))
        {
            GridManager.PlaceObjectOnTheGrid(this);
            waypointL.UpdateGridCoordinates();
            waypointR1.UpdateGridCoordinates();
            waypointR2.UpdateGridCoordinates();
            waypointR3.UpdateGridCoordinates();

            //int yWaypointOffset = GridHeight / 2;

            IGridObject west = GridManager.GetGridObject(GridX + GridHeight, GridY);
			if (west != null && west.GridX == GridX + GridHeight && west.GridY == GridY && (west is StraightRoadRotatedEastward || west is SingleTurnRight1 || west is SingleTurnRight))
            {
                Waypoint rWest = GridManager.GetWaypoint(waypointR1.gridX + 1, waypointR1.gridY);
                if (rWest != null)
                    rWest.AddWaypoint(waypointR1, 1);

                Waypoint lWest = GridManager.GetWaypoint(waypointL.gridX + 1, waypointL.gridY);
                if (lWest != null)
                    waypointL.AddWaypoint(lWest, 1);
            }
            IGridObject north = GridManager.GetGridObject(GridX, GridY - GridHeight);
			if (north != null && north.GridX == GridX && north.GridY == GridY - GridHeight && (north is StraightRoadRotatedNorthward || north is SingleTurnLeft || north is SingleTurnRight1))
            {
                Waypoint rNorth = GridManager.GetWaypoint(waypointR3.gridX, waypointR3.gridY - 1);
                if (rNorth != null)
                    waypointR3.AddWaypoint(rNorth, 1);

                Waypoint lNorth = GridManager.GetWaypoint(waypointL.gridX, waypointL.gridY - 1);
                if (lNorth != null)
                    lNorth.AddWaypoint(waypointL, 1);
            }
        }
    }

	public override void DrawGizmos(Color color)
	{
		waypointL.DrawGizmos(color);
		waypointR1.DrawGizmos(color);
		waypointR2.DrawGizmos(color);
		waypointR3.DrawGizmos(color);
	}
}
