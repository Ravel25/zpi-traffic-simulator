﻿using UnityEngine;
using System.Collections;

public class DoubleTurnRight1: Road {
	
	public Waypoint waypoint11, waypoint12, waypoint13, waypoint14,
	waypoint21, waypoint22, waypoint23, waypoint24,
	waypoint31, waypoint32, waypoint33, waypoint34,
	waypoint41, waypoint42, waypoint43, waypoint44;

	public override void UpdateGrid()
	{
		int[] coordinates = GridManager.GetGridCoordinates (this.transform.position);
		GridX = coordinates [0];
		GridY = coordinates [1];
		
		if (GridManager.IsSpotVacant (this)) {
			GridManager.PlaceObjectOnTheGrid (this);
			waypoint11.UpdateGridCoordinates (); 
			waypoint12.UpdateGridCoordinates (); 
			waypoint13.UpdateGridCoordinates (); 
			waypoint14.UpdateGridCoordinates ();
			waypoint21.UpdateGridCoordinates (); 
			waypoint22.UpdateGridCoordinates (); 
			waypoint23.UpdateGridCoordinates (); 
			waypoint24.UpdateGridCoordinates ();
			waypoint31.UpdateGridCoordinates (); 
			waypoint32.UpdateGridCoordinates ();
			waypoint33.UpdateGridCoordinates (); 
			waypoint34.UpdateGridCoordinates ();
			waypoint41.UpdateGridCoordinates (); 
			waypoint42.UpdateGridCoordinates (); 
			waypoint43.UpdateGridCoordinates (); 
			waypoint44.UpdateGridCoordinates ();

			
			
			IGridObject west = GridManager.GetGridObject (GridX + 4, GridY);
			if (west != null && west.GridX == GridX + 4 && west.GridY == GridY && (west is StraightDoubleRoadRotatedEastward || west is DoubleTurnLeft || west is DoubleTurnLeft1)) {
				Waypoint rlWest = GridManager.GetWaypoint (waypoint24.gridX + 1, waypoint24.gridY);
				if (rlWest != null)
                    rlWest.AddWaypoint(waypoint24, 1);
				
				Waypoint lrWest = GridManager.GetWaypoint (waypoint44.gridX + 1, waypoint44.gridY);
				if (lrWest != null)
                    waypoint44.AddWaypoint(lrWest, 1);
				
				Waypoint rrWest = GridManager.GetWaypoint (waypoint14.gridX + 1, waypoint14.gridY);
				if (rrWest != null)
                    rrWest.AddWaypoint(waypoint14, 1);
				
				Waypoint llWest = GridManager.GetWaypoint (waypoint34.gridX + 1, waypoint34.gridY);
				if (llWest != null)
                    waypoint34.AddWaypoint(llWest, 1);
				
			}
			
			IGridObject south = GridManager.GetGridObject (GridX, GridY + 4);
			if (south != null && south.GridX == GridX && south.GridY == GridY + 4 && (south is StraightDoubleRoadRotatedNorthward || south is DoubleTurnLeft || south is DoubleTurnRight)) {
				Waypoint rlSouth = GridManager.GetWaypoint (waypoint11.gridX, waypoint11.gridY + 1);
				if (rlSouth != null)
                    rlSouth.AddWaypoint(waypoint11, 1);
				
				Waypoint lrSouth = GridManager.GetWaypoint (waypoint13.gridX, waypoint13.gridY + 1);
				if (lrSouth != null)
                    waypoint13.AddWaypoint(lrSouth, 1);
				
				Waypoint rrSouth = GridManager.GetWaypoint (waypoint12.gridX, waypoint12.gridY + 1);
				if (rrSouth != null)
                    rrSouth.AddWaypoint(waypoint12, 1);
				
				Waypoint llSouth = GridManager.GetWaypoint (waypoint14.gridX, waypoint14.gridY + 1);
				if (llSouth != null)
                    waypoint14.AddWaypoint(llSouth, 1);
			}

		}
	}
	
	public override void DrawGizmos(Color color)
	{
		waypoint11.DrawGizmos(color); 
		waypoint12.DrawGizmos(color);  
		waypoint13.DrawGizmos(color);  
		waypoint14.DrawGizmos(color); 
		waypoint21.DrawGizmos(color); 
		waypoint22.DrawGizmos(color);  
		waypoint23.DrawGizmos(color);  
		waypoint24.DrawGizmos(color); 
		waypoint31.DrawGizmos(color); 
		waypoint32.DrawGizmos(color); 
		waypoint33.DrawGizmos(color); 
		waypoint34.DrawGizmos(color); 
		waypoint41.DrawGizmos(color); 
		waypoint42.DrawGizmos(color); 
		waypoint43.DrawGizmos(color); 
		waypoint44.DrawGizmos(color); 
	}
}
