﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Waypoint : MonoBehaviour
{
    public int gridX, gridY;
    public Waypoint[] availableWaypoints;
    public int[] availableWaypointsWeights;
    public Transform cachedTransform;
    public bool leftLane;



    void Awake()
    {
        cachedTransform = this.transform;
    }

    public void UpdateGridCoordinates()
    {
        if (cachedTransform == null)
            cachedTransform = this.transform;

        int[] gridCoordinates = GridManager.GetGridCoordinates(cachedTransform.position);
        gridX = gridCoordinates[0];
        gridY = gridCoordinates[1];
        GridManager.PlaceWaypointOnTheGrid(this);
    }


    public void AddWaypoint(Waypoint waypoint, int weight)
    {
        Waypoint[] newWaypointArray = new Waypoint[availableWaypoints.Length + 1];
        for (int i = 0; i < availableWaypoints.Length; i++)
        {
            newWaypointArray[i] = availableWaypoints[i];
        }
        newWaypointArray[newWaypointArray.Length - 1] = waypoint;
        availableWaypoints = newWaypointArray;

        int[] newWeightsArray = new int[availableWaypointsWeights.Length + 1];
        for (int i = 0; i < availableWaypointsWeights.Length; i++)
        {
            newWeightsArray[i] = availableWaypointsWeights[i];
        }
        newWeightsArray[newWeightsArray.Length - 1] = weight;
        availableWaypointsWeights = newWeightsArray;
    }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;

        Waypoint other = obj as Waypoint;
        return gridX == other.gridX && gridY == other.gridY;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public void DrawGizmos(Color color)
    {
        if (availableWaypoints != null)
        {
            if (cachedTransform == null)
            {
                Transform gizmoCachedTransform = this.transform;
                foreach (Waypoint waypoint in availableWaypoints)
                {
                    DrawArrow.ForDebug(gizmoCachedTransform.position, waypoint.transform.position - gizmoCachedTransform.position, color, 1f, 27f);
                }
            }
            else
            {
                foreach (Waypoint waypoint in availableWaypoints)
                {
                    DrawArrow.ForDebug(cachedTransform.position, waypoint.cachedTransform.position - cachedTransform.position, color, 1f, 27f);
                }
            }
        }
    }


}
