﻿using UnityEngine;
using System.Collections;

public class Crossing3 : Road
{
    public Waypoint waypointL1, waypointL2,
                    waypointR1, waypointR2;

    public override void DrawGizmos(Color color)
    {
        waypointL1.DrawGizmos(color);
        waypointL2.DrawGizmos(color);
        waypointR1.DrawGizmos(color);
        waypointR2.DrawGizmos(color);
    }

    public override void UpdateGrid()
    {
        throw new System.NotImplementedException();
    }
}