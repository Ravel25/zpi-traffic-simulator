﻿using UnityEngine;
using System.Collections;

public class DoubleCrossingSecond : DoubleCrossingAbstract {
    
  
    public override void Start()
    {
        _timePassedBy = interval;
        perm_interval = interval;

        transformCube = new Transform[trafficLights.Length];

        for (int i = 0; i < trafficLights.Length; i++)
        {
            transformCube[i] = trafficLights[i].transform;
        }
    }




    public override void Update()
    {
        _timePassedBy += Time.deltaTime;

        if (_timePassedBy > interval)
        {
            _timePassedBy = 0;

            if (state != 0)
            {
                lastState = state;
                state = 0;
            }
            else
            {
                switch (lastState)
                {
                    case 1:
                        state = 2;
                        break;
                    case 2:
                        state = 3;
                        break;
                    case 3:
                        state = 4;
                        break;

                    case 4:
                        state = 1;
                        break;
                }
            }

            // Debug.Log("Log " + state);
            Debug.Log(state);
            if (state == 0)
            {
                interval = closedTime;
            }
            else
            {
                interval = perm_interval;
            }

            if (state != 0)
            {
                //  Debug.Log("podnosze " + state);
                // cube[(i - 1) >= 0 ? i : 3].rigidbody.MovePosition(cube[(i - 1) >= 0 ? i : 3].rigidbody.position - (new Vector3(heigth, 0)));

                switch (state)
                {
                    case 1:
                        transformCube[1].position = (transformCube[1].transform.position + (new Vector3(0, heigth, 0)));//góra
                        transformCube[0].position = (transformCube[0].transform.position + (new Vector3(0, heigth, 0)));//góra

                        break;
                    case 2:
                        transformCube[2].position = (transformCube[2].transform.position + (new Vector3(0, heigth, 0)));//góra
                        transformCube[3].position = (transformCube[3].transform.position + (new Vector3(0, heigth, 0)));//góra

                        break;
                    case 3:
                        transformCube[4].position = (transformCube[4].transform.position + (new Vector3(0, heigth, 0)));//góra
                        transformCube[5].position = (transformCube[5].transform.position + (new Vector3(0, heigth, 0)));//góra

                        break;

                    case 4:
                        transformCube[6].position = (transformCube[6].transform.position + (new Vector3(0, heigth, 0)));//góra
                        transformCube[7].position = (transformCube[7].transform.position + (new Vector3(0, heigth, 0)));//góra

                        break;
                }

            }
            else
            {
                // Debug.Log("Opuszczam " + lastState);
                if (!first)
                {
                    switch (lastState)
                    {
                        case 1:
                            transformCube[1].position = (transformCube[1].transform.position - (new Vector3(0, heigth, 0)));//góra
                            transformCube[0].position = (transformCube[0].transform.position - (new Vector3(0, heigth, 0)));//góra

                            break;
                        case 2:
                            transformCube[2].position = (transformCube[2].transform.position - (new Vector3(0, heigth, 0)));//góra
                            transformCube[3].position = (transformCube[3].transform.position - (new Vector3(0, heigth, 0)));//góra

                            break;
                        case 3:
                            transformCube[4].position = (transformCube[4].transform.position - (new Vector3(0, heigth, 0)));//góra
                            transformCube[5].position = (transformCube[5].transform.position - (new Vector3(0, heigth, 0)));//góra

                            break;

                        case 4:
                            transformCube[6].position = (transformCube[6].transform.position - (new Vector3(0, heigth, 0)));//góra
                            transformCube[7].position = (transformCube[7].transform.position - (new Vector3(0, heigth, 0)));//góra

                            break;
                    }
                }
                else
                {
                    first = false;
                }
            }


        }
    }


    
}
