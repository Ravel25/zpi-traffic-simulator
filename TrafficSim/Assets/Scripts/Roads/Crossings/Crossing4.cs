﻿using UnityEngine;
using System.Collections;

public class Crossing4 : Road
{
    public Waypoint waypointL1, waypointL2,
                    waypointR1, waypointR2;

    public GameObject[] trafficLights;
    public Transform[] transformCube;
    public Material greenLightMaterial, redLightMaterial;
    public float trafficLightSize;
    public int waypointsAdvanceToWait;

    private float _timePassedBy;
    public float interval;
    private float perm_interval;
    int state = 1;                      //0 oba zamknięte, 1/-1 jedno otwarte
    public float heigth;
    private bool first = true;
    private int lastState;
    public float closedTime = 2;

    Vector3 _trafficLightTranslateVector;


    void Start()
    {
        _timePassedBy = interval;
        perm_interval = interval;

        transformCube = new Transform[trafficLights.Length];

        for (int i = 0; i < trafficLights.Length; i++)
        {
            transformCube[i] = trafficLights[i].transform;    
        }

        _trafficLightTranslateVector = new Vector3(0, heigth, 0);
    }

    

    void Update()
    {
        _timePassedBy += Time.deltaTime;

        if (_timePassedBy > interval)
        {
            _timePassedBy = 0;
            if (state != 0)
            {
                lastState = state;
            }

            state = ((state == 1) || (state == -1)) ? 0 : 0 - lastState;
            // Debug.Log("Log " + state);

            if (state == 0)
            {
                interval = closedTime;
            }
            else
            {
                interval = perm_interval;
            }

            if (state != 0)
            {
                //  Debug.Log("podnosze " + state);
                // cube[(i - 1) >= 0 ? i : 3].rigidbody.MovePosition(cube[(i - 1) >= 0 ? i : 3].rigidbody.position - (new Vector3(heigth, 0)));
                //transformCube[state == 1 ? 0 : 2].position = transformCube[state == 1 ? 0 : 2].position + (new Vector3(0, heigth, 0));//góra
                //transformCube[state == 1 ? 1 : 3].position = transformCube[state == 1 ? 1 : 3].position + (new Vector3(0, heigth, 0));//góra

                transformCube[state == 1 ? 0 : 2].Translate(_trafficLightTranslateVector);
                transformCube[state == 1 ? 1 : 3].Translate(_trafficLightTranslateVector);

                trafficLights[state == 1 ? 0 : 2].renderer.material = greenLightMaterial;
                trafficLights[state == 1 ? 1 : 3].renderer.material = greenLightMaterial;

                trafficLights[state == 1 ? 0 : 2].renderer.enabled = false;
                trafficLights[state == 1 ? 1 : 3].renderer.enabled = false;

            }
            else
            {
                // Debug.Log("Opuszczam " + lastState);
                if (!first)
                {
                    //transformCube[lastState == 1 ? 0 : 2].position = transformCube[lastState == 1 ? 0 : 2].position - (new Vector3(0, heigth, 0));   //dół
                    //transformCube[lastState == 1 ? 1 : 3].position = transformCube[lastState == 1 ? 1 : 3].position - (new Vector3(0, heigth, 0));

                    transformCube[lastState == 1 ? 0 : 2].Translate(-_trafficLightTranslateVector);
                    transformCube[lastState == 1 ? 1 : 3].Translate(-_trafficLightTranslateVector);


                    trafficLights[lastState == 1 ? 0 : 2].renderer.material = redLightMaterial;
                    trafficLights[lastState == 1 ? 1 : 3].renderer.material = redLightMaterial;

                    trafficLights[lastState == 1 ? 0 : 2].renderer.enabled = true;
                    trafficLights[lastState == 1 ? 1 : 3].renderer.enabled = true;
                }
                else
                {
                    first = false;
                }
            }


        }
        
        
    }


    public override void UpdateGrid()
    {
        int[] coordinates = GridManager.GetGridCoordinates(this.transform.position);
        GridX = coordinates[0];
        GridY = coordinates[1];

        if (GridManager.IsSpotVacant(this))
        {
            GridManager.PlaceObjectOnTheGrid(this);
            waypointL1.UpdateGridCoordinates();
            waypointL2.UpdateGridCoordinates();
            waypointR1.UpdateGridCoordinates();
            waypointR2.UpdateGridCoordinates();


            IGridObject east = GridManager.GetGridObject(GridX + GridWidth, GridY);

            if (east != null && east.GridX == GridX + GridWidth && east.GridY == GridY && (east is StraightRoadRotatedEastward || east is Crossing4 || east is StraightDoubleRoadRotatedEastward))
            {
                Waypoint w1 = GridManager.GetWaypoint(waypointR1.gridX + 1, waypointR1.gridY);
                if (w1 != null)
                    waypointR1.AddWaypoint(w1, 1);

                Waypoint w2 = GridManager.GetWaypoint(waypointR2.gridX + 1, waypointR2.gridY);
                if (w2 != null)
                    w2.AddWaypoint(waypointR2, 1);
            }


            IGridObject north = GridManager.GetGridObject(GridX, GridY + GridHeight);
            if (north != null && north.GridX == GridX && north.GridY == GridY + GridHeight && (north is StraightRoadRotatedNorthward || north is Crossing4 || north is StraightDoubleRoadRotatedNorthward))
            {
                Waypoint w1 = GridManager.GetWaypoint(waypointR2.gridX, waypointR2.gridY + 1);
                if (w1 != null)
                    waypointR2.AddWaypoint(w1, 1);

                Waypoint w2 = GridManager.GetWaypoint(waypointL1.gridX, waypointL1.gridY + 1);
                if (w2 != null)
                    w2.AddWaypoint(waypointL1, 1);
            }


            IGridObject west = GridManager.GetGridObject(GridX - GridWidth, GridY);
            if (west != null && west.GridX == GridX - GridWidth && west.GridY == GridY && (west is StraightRoadRotatedEastward || west is Crossing4))
            {
                Waypoint w1 = GridManager.GetWaypoint(waypointL1.gridX - 1, waypointL1.gridY);
                if (w1 != null)
                    waypointL1.AddWaypoint(w1, 1);

                Waypoint w2 = GridManager.GetWaypoint(waypointL2.gridX - 1, waypointL2.gridY);
                if (w2 != null)
                    w2.AddWaypoint(waypointL2, 1);
            }

            IGridObject south = GridManager.GetGridObject(GridX, GridY - GridHeight);
            if (south != null && south.GridX == GridX && south.GridY == GridY - GridHeight && (south is StraightRoadRotatedNorthward || south is Crossing4))
            {
                Waypoint w1 = GridManager.GetWaypoint(waypointR1.gridX, waypointR1.gridY - 1);
                if (w1 != null)
                    w1.AddWaypoint(waypointR1, 1);

                Waypoint w2 = GridManager.GetWaypoint(waypointL2.gridX, waypointL2.gridY - 1);
                if (w2 != null)
                    waypointL2.AddWaypoint(w2, 1);
            }


        }
    }

    void OnTriggerEnter(Collider collider)
    {
        VehicleMemory vehicle = collider.GetComponent<VehicleMemory>();
        if (vehicle != null)
        {
            VehicleEnterCrossing(vehicle);
        }
    }

    void OnTriggerExit(Collider collider)
    {
        VehicleMemory vehicle = collider.GetComponent<VehicleMemory>();
        if (vehicle != null)
        {
            VehicleExitCrossing(vehicle);
        }
    }


    void VehicleEnterCrossing(VehicleMemory vehicle)
    {
        vehicle.speedLimitEnabled = false;
        if (vehicle.isTurnAhead && vehicle.isTurnLeftAhead)
        {
            vehicle.isOnCrossing = true;
            vehicle.canLeaveCrossing = false;
            vehicle.crossingInitialWaypointIndex = vehicle._currentWaypointIndex;
            vehicle.crossingWaypointsAdvance = waypointsAdvanceToWait;
        }
        else
        {
            vehicle.canLeaveCrossing = true;
        }
    }

    void VehicleExitCrossing(VehicleMemory vehicle)
    {
        vehicle.isOnCrossing = false;
        vehicle.canLeaveCrossing = false;
        vehicle.reversing = false;

    }

    public override void DrawGizmos(Color color)
    {
        waypointL1.DrawGizmos(color);
        waypointL2.DrawGizmos(color);
        waypointR1.DrawGizmos(color);
        waypointR2.DrawGizmos(color);
    }

    //void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.gray;
    //    foreach (GameObject trafficLight in trafficLights)
    //    {
    //        if (trafficLight.transform.position.y > heigth)
    //            Gizmos.color = Color.green;
    //        else
    //            Gizmos.color = Color.red;

    //        Gizmos.DrawSphere(trafficLight.transform.position, trafficLightSize);
    //    }
    //}
}