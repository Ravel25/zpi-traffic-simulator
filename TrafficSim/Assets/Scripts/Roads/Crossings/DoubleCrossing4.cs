﻿using UnityEngine;
using System.Collections;

public class DoubleCrossing4 : DoubleCrossingAbstract
{
    public int waypointsAdvanceToWait = 2;


    public override void Start()
    {
        _timePassedBy = interval;
        perm_interval = interval;

        transformCube = new Transform[trafficLights.Length];

        for (int i = 0; i < trafficLights.Length; i++)
        {
            transformCube[i] = trafficLights[i].transform;
        }
    }




    public override void Update()
    {
        _timePassedBy += Time.deltaTime;

        if (_timePassedBy > interval)
        {
            _timePassedBy = 0;

            if (state != 0)
            {
                lastState = state;
                state = 0;
            }
            else
            {
                switch (lastState)
                {
                    case 1:
                        state = 2;
                        break;
                    case 2:
                        state = 1;
                        break;

                }
            }

            // Debug.Log("Log " + state);
            //Debug.Log(state);
            if (state == 0)
            {
                interval = closedTime;
            }
            else
            {
                interval = perm_interval;
            }

            if (state != 0)
            {
                //  Debug.Log("podnosze " + state);
                // cube[(i - 1) >= 0 ? i : 3].rigidbody.MovePosition(cube[(i - 1) >= 0 ? i : 3].rigidbody.position - (new Vector3(heigth, 0)));

                switch (state)
                {
                    case 1:
                        transformCube[1].position = (transformCube[1].position + (new Vector3(0, heigth, 0)));//góra
                        transformCube[0].position = (transformCube[0].position + (new Vector3(0, heigth, 0)));//góra

                        transformCube[2].position = (transformCube[2].position + (new Vector3(0, heigth, 0)));//góra
                        transformCube[3].position = (transformCube[3].position + (new Vector3(0, heigth, 0)));//góra

                        trafficLights[1].renderer.material = greenLightMaterial;
                        trafficLights[0].renderer.material = greenLightMaterial;

                        trafficLights[2].renderer.material = greenLightMaterial;
                        trafficLights[3].renderer.material = greenLightMaterial;


                        trafficLights[1].renderer.enabled = false;
                        trafficLights[0].renderer.enabled = false;

                        trafficLights[2].renderer.enabled = false;
                        trafficLights[3].renderer.enabled = false;


                        break;
                    case 2:
                        transformCube[4].position = (transformCube[4].position + (new Vector3(0, heigth, 0)));//góra
                        transformCube[5].position = (transformCube[5].position + (new Vector3(0, heigth, 0)));//góra

                        transformCube[6].position = (transformCube[6].position + (new Vector3(0, heigth, 0)));//góra
                        transformCube[7].position = (transformCube[7].position + (new Vector3(0, heigth, 0)));//góra

                        trafficLights[4].renderer.material = greenLightMaterial;
                        trafficLights[5].renderer.material = greenLightMaterial;

                        trafficLights[6].renderer.material = greenLightMaterial;
                        trafficLights[7].renderer.material = greenLightMaterial;


                        trafficLights[4].renderer.enabled = false;
                        trafficLights[5].renderer.enabled = false;

                        trafficLights[6].renderer.enabled = false;
                        trafficLights[7].renderer.enabled = false;

                        break;
                }

            }
            else
            {
                // Debug.Log("Opuszczam " + lastState);
                if (!first)
                {
                    switch (lastState)
                    {
                        case 1:
                            transformCube[1].position = (transformCube[1].transform.position - (new Vector3(0, heigth, 0)));//góra
                            transformCube[0].position = (transformCube[0].transform.position - (new Vector3(0, heigth, 0)));//góra

                            transformCube[2].position = (transformCube[2].transform.position - (new Vector3(0, heigth, 0)));//góra
                            transformCube[3].position = (transformCube[3].transform.position - (new Vector3(0, heigth, 0)));//góra

                            trafficLights[1].renderer.material = redLightMaterial;
                            trafficLights[0].renderer.material = redLightMaterial;

                            trafficLights[2].renderer.material = redLightMaterial;
                            trafficLights[3].renderer.material = redLightMaterial;

                            trafficLights[1].renderer.enabled = true;
                            trafficLights[0].renderer.enabled = true;

                            trafficLights[2].renderer.enabled = true;
                            trafficLights[3].renderer.enabled = true;


                            break;
                        case 2:
                            transformCube[4].position = (transformCube[4].position - (new Vector3(0, heigth, 0)));//góra
                            transformCube[5].position = (transformCube[5].position - (new Vector3(0, heigth, 0)));//góra

                            transformCube[6].position = (transformCube[6].position - (new Vector3(0, heigth, 0)));//góra
                            transformCube[7].position = (transformCube[7].position - (new Vector3(0, heigth, 0)));//góra

                            trafficLights[4].renderer.material = redLightMaterial;
                            trafficLights[5].renderer.material = redLightMaterial;

                            trafficLights[6].renderer.material = redLightMaterial;
                            trafficLights[7].renderer.material = redLightMaterial;


                            trafficLights[4].renderer.enabled = true;
                            trafficLights[5].renderer.enabled = true;

                            trafficLights[6].renderer.enabled = true;
                            trafficLights[7].renderer.enabled = true;

                            break;

                    }
                }
                else
                {
                    first = false;
                }
            }


        }
    }

    void OnTriggerEnter(Collider collider)
    {
        VehicleMemory vehicle = collider.GetComponent<VehicleMemory>();
        if (vehicle != null)
        {
            VehicleEnterCrossing(vehicle);
        }
    }

    void OnTriggerExit(Collider collider)
    {
        VehicleMemory vehicle = collider.GetComponent<VehicleMemory>();
        if (vehicle != null)
        {
            VehicleExitCrossing(vehicle);
        }
    }


    void VehicleEnterCrossing(VehicleMemory vehicle)
    {
        vehicle.speedLimitEnabled = false;
        if (vehicle.isTurnAhead && vehicle.isTurnLeftAhead)
        {
            //vehicle.isOnDoubleLaneCrossing = true;
            vehicle.isOnCrossing = true;
            vehicle.canLeaveCrossing = false;
            vehicle.crossingInitialWaypointIndex = vehicle._currentWaypointIndex;
            vehicle.crossingWaypointsAdvance = waypointsAdvanceToWait;
        }
        else
        {
            vehicle.canLeaveCrossing = true;
        }
    }

    void VehicleExitCrossing(VehicleMemory vehicle)
    {
        //vehicle.isOnDoubleLaneCrossing = false;
        vehicle.isOnCrossing = false;
        vehicle.canLeaveCrossing = false;
        vehicle.reversing = false;

    }



}
