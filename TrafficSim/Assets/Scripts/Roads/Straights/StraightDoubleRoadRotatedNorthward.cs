﻿using UnityEngine;
using System.Collections;

public class StraightDoubleRoadRotatedNorthward : StraightDoubleRoad 
{
	
	override public void UpdateGrid()
	{
		int[] coordinates = GridManager.GetGridCoordinates(this.transform.position);
		GridX = coordinates[0];
		GridY = coordinates[1];

		if (GridManager.IsSpotVacant(this))
		{
			GridManager.PlaceObjectOnTheGrid(this);
			waypointLeftLeftHead.UpdateGridCoordinates();
			waypointLeftLeftTail.UpdateGridCoordinates();
			waypointLeftRightHead.UpdateGridCoordinates();
			waypointLeftRightTail.UpdateGridCoordinates();
			waypointRightLeftHead.UpdateGridCoordinates();
			waypointRightLeftTail.UpdateGridCoordinates();
			waypointRightRightHead.UpdateGridCoordinates();
			waypointRightRightTail.UpdateGridCoordinates();
			
			
			
			int yWaypointOffset = GridHeight / 2;
			

			IGridObject north = GridManager.GetGridObject(GridX, GridY + GridHeight);
			if (north != null && north.GridX == GridX && north.GridY == GridY + GridHeight  && (north is StraightDoubleRoadRotatedNorthward))
			{	
				Waypoint rlNorth = GridManager.GetWaypoint(waypointRightLeftHead.gridX, waypointRightLeftHead.gridY + yWaypointOffset);
				if (rlNorth != null)
					waypointRightLeftHead.AddWaypoint(rlNorth, 1);

				Waypoint lrNorth = GridManager.GetWaypoint(waypointLeftRightTail.gridX, waypointLeftRightTail.gridY + yWaypointOffset);
				if (lrNorth != null)
                    lrNorth.AddWaypoint(waypointLeftRightTail, 1);

				Waypoint rrNorth = GridManager.GetWaypoint(waypointRightRightHead.gridX, waypointRightRightHead.gridY + yWaypointOffset);
				if (rrNorth != null)
                    waypointRightRightHead.AddWaypoint(rrNorth, 1);

				Waypoint llNorth = GridManager.GetWaypoint(waypointLeftLeftTail.gridX, waypointLeftLeftTail.gridY + yWaypointOffset);
				if (llNorth != null)
                    llNorth.AddWaypoint(waypointLeftLeftTail, 1);

				Waypoint rrNorth2 = GridManager.GetWaypoint(waypointRightRightHead.gridX-1, waypointRightRightHead.gridY + yWaypointOffset);
				if (rrNorth2 != null)
                    waypointRightRightHead.AddWaypoint(rrNorth2, 2);

				Waypoint lrNorth2 = GridManager.GetWaypoint(waypointLeftRightTail.gridX-1, waypointLeftRightTail.gridY + yWaypointOffset);
				if (lrNorth2 != null)
                    lrNorth2.AddWaypoint(waypointLeftRightTail, 2);

				Waypoint rlNorth2 = GridManager.GetWaypoint(waypointRightLeftHead.gridX+yWaypointOffset, waypointRightLeftHead.gridY + yWaypointOffset);
				if (rlNorth2 != null)
                    waypointRightLeftHead.AddWaypoint(rlNorth2, 2);

				Waypoint llNorth2 = GridManager.GetWaypoint(waypointLeftLeftTail.gridX+1, waypointLeftLeftTail.gridY + yWaypointOffset);
				if (llNorth2 != null)
                    llNorth2.AddWaypoint(waypointLeftLeftTail, 2);


			}
		
			IGridObject south = GridManager.GetGridObject(GridX, GridY - GridHeight);
			if (south != null && south.GridX == GridX && south.GridY == GridY - GridHeight && (south is StraightDoubleRoadRotatedNorthward))
			{
				Waypoint rlSouth = GridManager.GetWaypoint(waypointRightLeftTail.gridX, waypointRightLeftTail.gridY - yWaypointOffset);
				if (rlSouth != null)
                    rlSouth.AddWaypoint(waypointRightLeftTail, 1);
		
				Waypoint lrSouth = GridManager.GetWaypoint(waypointLeftRightHead.gridX, waypointLeftRightHead.gridY - yWaypointOffset);
				if (lrSouth != null)
                    waypointLeftRightHead.AddWaypoint(lrSouth, 1);

				Waypoint rrSouth = GridManager.GetWaypoint(waypointRightRightTail.gridX, waypointRightRightTail.gridY - yWaypointOffset);
				if (rrSouth != null)
					rrSouth.AddWaypoint(waypointRightRightTail, 1);

				Waypoint llSouth = GridManager.GetWaypoint(waypointLeftLeftHead.gridX, waypointLeftLeftHead.gridY - yWaypointOffset);
				if (llSouth != null)
					waypointLeftLeftHead.AddWaypoint(llSouth, 1);

				Waypoint lrSouth2 = GridManager.GetWaypoint(waypointLeftRightHead.gridX-1, waypointLeftRightHead.gridY - yWaypointOffset);
				if (lrSouth2!= null)
                    waypointLeftRightHead.AddWaypoint(lrSouth2, 2);

				Waypoint rrSouth2 = GridManager.GetWaypoint(waypointRightRightTail.gridX-1, waypointRightRightTail.gridY - yWaypointOffset);
				if (rrSouth2 != null)
                    rrSouth2.AddWaypoint(waypointRightRightTail, 2);

				Waypoint rlSouth2 = GridManager.GetWaypoint(waypointRightLeftTail.gridX+1, waypointRightLeftTail.gridY - yWaypointOffset);
				if (rlSouth2 != null)
                    rlSouth2.AddWaypoint(waypointRightLeftTail, 2);

				Waypoint llSouth2 = GridManager.GetWaypoint(waypointLeftLeftHead.gridX+1, waypointLeftLeftHead.gridY - yWaypointOffset);
				if (llSouth2 != null)
					waypointLeftLeftHead.AddWaypoint(llSouth2, 2);
			}

			IGridObject southSingle = GridManager.GetGridObject(GridX+1, GridY - GridHeight);
			if (southSingle != null && southSingle.GridX == GridX+1 && southSingle.GridY == GridY - GridHeight && (southSingle is StraightRoadRotatedNorthward))
			{
				Waypoint rlSouth = GridManager.GetWaypoint(waypointRightLeftTail.gridX, waypointRightLeftTail.gridY - yWaypointOffset);
				if (rlSouth != null)
					rlSouth.AddWaypoint(waypointRightLeftTail, 1);
				
				Waypoint lrSouth = GridManager.GetWaypoint(waypointLeftRightHead.gridX, waypointLeftRightHead.gridY - yWaypointOffset);
				if (lrSouth != null)
                    waypointLeftRightHead.AddWaypoint(lrSouth, 1);
			
				Waypoint rrSouth = GridManager.GetWaypoint(waypointRightRightTail.gridX-1, waypointRightRightTail.gridY - yWaypointOffset);
				if (rrSouth != null)
					rrSouth.AddWaypoint(waypointRightRightTail, 2);

				Waypoint llSouth = GridManager.GetWaypoint(waypointLeftLeftHead.gridX+1, waypointLeftLeftHead.gridY - yWaypointOffset);
				if (llSouth != null)
					waypointLeftLeftHead.AddWaypoint(llSouth, 2);
			}

			IGridObject northSingle = GridManager.GetGridObject(GridX+1, GridY + GridHeight);
			if (northSingle != null && northSingle.GridX == GridX+1 && northSingle.GridY == GridY + GridHeight  && (northSingle is StraightRoadRotatedNorthward))
			{	
				Waypoint rlNorth = GridManager.GetWaypoint(waypointRightLeftHead.gridX, waypointRightLeftHead.gridY + yWaypointOffset);
				if (rlNorth != null)
                    waypointRightLeftHead.AddWaypoint(rlNorth, 1);
				
				Waypoint lrNorth = GridManager.GetWaypoint(waypointLeftRightTail.gridX, waypointLeftRightTail.gridY + yWaypointOffset);
				if (lrNorth != null)
                    lrNorth.AddWaypoint(waypointLeftRightTail, 1);
				
				Waypoint rrNorth = GridManager.GetWaypoint(waypointRightRightHead.gridX-1, waypointRightRightHead.gridY + yWaypointOffset);
				if (rrNorth != null)
                    waypointRightRightHead.AddWaypoint(rrNorth, 2);
				
				Waypoint llNorth = GridManager.GetWaypoint(waypointLeftLeftTail.gridX+1, waypointLeftLeftTail.gridY + yWaypointOffset);
				if (llNorth != null)
                    llNorth.AddWaypoint(waypointLeftLeftTail, 2);
			}

			IGridObject northCrossing = GridManager.GetGridObject(GridX, GridY +2);
			if (northCrossing != null && northCrossing.GridX == GridX && northCrossing.GridY == GridY +2  && (northCrossing is DoubleCrossing4 || northCrossing is DoubleTurnLeft || northCrossing is DoubleTurnRight))
			{
				Waypoint rlNorth = GridManager.GetWaypoint(waypointRightLeftHead.gridX, waypointRightLeftHead.gridY + yWaypointOffset);
				if (rlNorth != null)
					waypointRightLeftHead.AddWaypoint(rlNorth, 1);
				
				Waypoint lrNorth = GridManager.GetWaypoint(waypointLeftRightTail.gridX, waypointLeftRightTail.gridY + yWaypointOffset);
				if (lrNorth != null)
                    lrNorth.AddWaypoint(waypointLeftRightTail, 1);
				
				Waypoint rrNorth = GridManager.GetWaypoint(waypointRightRightHead.gridX, waypointRightRightHead.gridY + yWaypointOffset);
				if (rrNorth != null)
                    waypointRightRightHead.AddWaypoint(rrNorth, 1);
				
				Waypoint llNorth = GridManager.GetWaypoint(waypointLeftLeftTail.gridX, waypointLeftLeftTail.gridY + yWaypointOffset);
				if (llNorth != null)
                    llNorth.AddWaypoint(waypointLeftLeftTail, 1);
			}
			
			IGridObject southCrossing = GridManager.GetGridObject(GridX, GridY - 4);
			if (southCrossing != null && southCrossing.GridX == GridX && southCrossing.GridY == GridY -4 && (southCrossing is DoubleCrossing4 || southCrossing is DoubleTurnLeft1 || southCrossing is DoubleTurnRight1))
			{
				Waypoint rlSouth = GridManager.GetWaypoint(waypointRightLeftTail.gridX, waypointRightLeftTail.gridY - yWaypointOffset);
				if (rlSouth != null)
                    rlSouth.AddWaypoint(waypointRightLeftTail, 1);
				
				Waypoint lrSouth = GridManager.GetWaypoint(waypointLeftRightHead.gridX, waypointLeftRightHead.gridY - yWaypointOffset);
				if (lrSouth != null)
                    waypointLeftRightHead.AddWaypoint(lrSouth, 1);
				
				Waypoint rrSouth = GridManager.GetWaypoint(waypointRightRightTail.gridX, waypointRightRightTail.gridY - yWaypointOffset);
				if (rrSouth != null)
                    rrSouth.AddWaypoint(waypointRightRightTail, 1);
				
				Waypoint llSouth = GridManager.GetWaypoint(waypointLeftLeftHead.gridX, waypointLeftLeftHead.gridY - yWaypointOffset);
				if (llSouth != null)
                    waypointLeftLeftHead.AddWaypoint(llSouth, 1);
			}
		}
	}
}
