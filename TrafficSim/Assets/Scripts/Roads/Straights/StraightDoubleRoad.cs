﻿using UnityEngine;
using System.Collections;

public abstract class StraightDoubleRoad : Road
{
    public Waypoint waypointLeftRightHead,
        waypointLeftRightTail,
        waypointLeftLeftHead,
        waypointLeftLeftTail,
        waypointRightRightHead,
        waypointRightRightTail,
        waypointRightLeftHead,
        waypointRightLeftTail;

    public override void DrawGizmos(Color color)
	{
        waypointLeftRightHead.DrawGizmos(color);
        waypointLeftRightTail.DrawGizmos(color);
        waypointLeftLeftHead.DrawGizmos(color);
        waypointLeftLeftTail.DrawGizmos(color);
        waypointRightRightHead.DrawGizmos(color);
        waypointRightRightTail.DrawGizmos(color);
        waypointRightLeftHead.DrawGizmos(color);
        waypointRightLeftTail.DrawGizmos(color);
    }
}
