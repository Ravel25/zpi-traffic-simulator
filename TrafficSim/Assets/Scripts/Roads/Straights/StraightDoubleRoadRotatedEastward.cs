﻿using UnityEngine;
using System.Collections;

public class StraightDoubleRoadRotatedEastward : StraightDoubleRoad
{
	override public void UpdateGrid()
	{
		int[] coordinates = GridManager.GetGridCoordinates(this.transform.position);
		GridX = coordinates[0];
		GridY = coordinates[1];

		if (GridManager.IsSpotVacant(this))
		{
			GridManager.PlaceObjectOnTheGrid(this);
			waypointLeftLeftHead.UpdateGridCoordinates();
			waypointLeftLeftTail.UpdateGridCoordinates();
			waypointLeftRightHead.UpdateGridCoordinates();
			waypointLeftRightTail.UpdateGridCoordinates();
			waypointRightLeftHead.UpdateGridCoordinates();
			waypointRightLeftTail.UpdateGridCoordinates();
			waypointRightRightHead.UpdateGridCoordinates();
			waypointRightRightTail.UpdateGridCoordinates();
			
			
			int xWaypointOffset = GridWidth / 2;

			IGridObject east = GridManager.GetGridObject(GridX + GridWidth, GridY);
			if (east != null && east.GridX == GridX + GridWidth && east.GridY == GridY && (east is StraightDoubleRoadRotatedEastward ))
			{
				Waypoint rlEast = GridManager.GetWaypoint(waypointRightLeftHead.gridX + xWaypointOffset, waypointRightLeftHead.gridY);
				if (rlEast != null)
                    waypointRightLeftHead.AddWaypoint(rlEast, 1);

				Waypoint lrEast = GridManager.GetWaypoint(waypointLeftRightTail.gridX + xWaypointOffset, waypointLeftRightTail.gridY);
				if (lrEast != null)
                    lrEast.AddWaypoint(waypointLeftRightTail, 1);

				Waypoint rrEast = GridManager.GetWaypoint(waypointRightRightHead.gridX + xWaypointOffset, waypointRightRightHead.gridY);
				if (rrEast != null)
                    waypointRightRightHead.AddWaypoint(rrEast, 1);

				Waypoint llEast = GridManager.GetWaypoint(waypointLeftLeftTail.gridX + xWaypointOffset, waypointLeftLeftTail.gridY);
				if (llEast != null)
                    llEast.AddWaypoint(waypointLeftLeftTail, 1);

				Waypoint lrEast2 = GridManager.GetWaypoint(waypointLeftRightTail.gridX + xWaypointOffset, waypointLeftRightTail.gridY+1);
				if (lrEast2 != null)
                    lrEast2.AddWaypoint(waypointLeftRightTail, 2);

				Waypoint rrEast2 = GridManager.GetWaypoint(waypointRightRightHead.gridX + xWaypointOffset, waypointRightRightHead.gridY+1);
				if (rrEast2 != null)
                    waypointRightRightHead.AddWaypoint(rrEast2, 2);

				Waypoint rlEast2 = GridManager.GetWaypoint(waypointRightLeftHead.gridX + xWaypointOffset, waypointRightLeftHead.gridY-1);
				if (rlEast2 != null)
                    waypointRightLeftHead.AddWaypoint(rlEast2, 2);

				Waypoint llEast2 = GridManager.GetWaypoint(waypointLeftLeftTail.gridX + xWaypointOffset, waypointLeftLeftTail.gridY-1);
				if (llEast2 != null)
                    llEast2.AddWaypoint(waypointLeftLeftTail, 2);
			}

			IGridObject eastSingle = GridManager.GetGridObject(GridX + 2, GridY+1);
			if (eastSingle != null && eastSingle.GridX == GridX + 2 && eastSingle.GridY == GridY+1 && (eastSingle is StraightRoadRotatedEastward))
			{

				Waypoint rlEast = GridManager.GetWaypoint(waypointRightLeftHead.gridX + xWaypointOffset, waypointRightLeftHead.gridY);
				if (rlEast != null)
					waypointRightLeftHead.AddWaypoint(rlEast, 1);
				
				Waypoint lrEast = GridManager.GetWaypoint(waypointLeftRightTail.gridX + xWaypointOffset, waypointLeftRightTail.gridY);
				if (lrEast != null)
                    lrEast.AddWaypoint(waypointLeftRightTail, 1);
				
				Waypoint rrEast = GridManager.GetWaypoint(waypointRightRightHead.gridX + xWaypointOffset, waypointRightRightHead.gridY+1);
				if (rrEast != null)
                    waypointRightRightHead.AddWaypoint(rrEast, 2);
				
				Waypoint llEast = GridManager.GetWaypoint(waypointLeftLeftTail.gridX + xWaypointOffset, waypointLeftLeftTail.gridY-1);
				if (llEast != null)
                    llEast.AddWaypoint(waypointLeftLeftTail, 2);
			}

			IGridObject westCrossing = GridManager.GetGridObject(GridX - 4, GridY);
			if (westCrossing != null && westCrossing.GridX == GridX - 4 && westCrossing.GridY == GridY && (westCrossing is DoubleCrossing4 || westCrossing is DoubleTurnRight || westCrossing is DoubleTurnRight1))
			{

				Waypoint rlWest = GridManager.GetWaypoint(waypointRightLeftTail.gridX - xWaypointOffset, waypointRightLeftTail.gridY);
				if (rlWest != null)
					rlWest.AddWaypoint(waypointRightLeftTail, 1);
				
				Waypoint lrWest = GridManager.GetWaypoint(waypointLeftRightHead.gridX - xWaypointOffset, waypointLeftRightHead.gridY);
				if (lrWest != null)
                    waypointLeftRightHead.AddWaypoint(lrWest, 1);
				
				Waypoint rrWest = GridManager.GetWaypoint(waypointRightRightTail.gridX - xWaypointOffset, waypointRightRightTail.gridY);
				if (rrWest != null)
                    rrWest.AddWaypoint(waypointRightRightTail, 1);
				
				Waypoint llWest = GridManager.GetWaypoint(waypointLeftLeftHead.gridX - xWaypointOffset, waypointLeftLeftHead.gridY);
				if (llWest != null)
                    waypointLeftLeftHead.AddWaypoint(llWest, 1);
			}

			IGridObject eastCrossing = GridManager.GetGridObject(GridX + 2, GridY);
			if (eastCrossing != null && eastCrossing.GridX == GridX + 2 && eastCrossing.GridY == GridY && (eastCrossing is DoubleCrossing4 || eastCrossing is DoubleTurnLeft || eastCrossing is DoubleTurnLeft1))
			{
				Waypoint rlEast = GridManager.GetWaypoint(waypointRightLeftHead.gridX + xWaypointOffset, waypointRightLeftHead.gridY);
				if (rlEast != null)
                    waypointRightLeftHead.AddWaypoint(rlEast, 1);
				
				Waypoint lrEast = GridManager.GetWaypoint(waypointLeftRightTail.gridX + xWaypointOffset, waypointLeftRightTail.gridY);
				if (lrEast != null)
                    lrEast.AddWaypoint(waypointLeftRightTail, 1);
				
				Waypoint rrEast = GridManager.GetWaypoint(waypointRightRightHead.gridX + xWaypointOffset, waypointRightRightHead.gridY);
				if (rrEast != null)
                    waypointRightRightHead.AddWaypoint(rrEast, 1);
				
				Waypoint llEast = GridManager.GetWaypoint(waypointLeftLeftTail.gridX + xWaypointOffset, waypointLeftLeftTail.gridY);
				if (llEast != null)
                    llEast.AddWaypoint(waypointLeftLeftTail, 1);
			}

			IGridObject westSingle = GridManager.GetGridObject(GridX - GridWidth, GridY+1);
			if (westSingle != null && westSingle.GridX == GridX - GridWidth && westSingle.GridY == GridY+1 && (westSingle is StraightRoadRotatedEastward))
			{
				Waypoint rlWest = GridManager.GetWaypoint(waypointRightLeftTail.gridX - xWaypointOffset, waypointRightLeftTail.gridY);
				if (rlWest != null)
                    rlWest.AddWaypoint(waypointRightLeftTail, 1);
				
				Waypoint lrWest = GridManager.GetWaypoint(waypointLeftRightHead.gridX - xWaypointOffset, waypointLeftRightHead.gridY);
				if (lrWest != null)
                    waypointLeftRightHead.AddWaypoint(lrWest, 1);
				
				Waypoint rrWest = GridManager.GetWaypoint(waypointRightRightTail.gridX - xWaypointOffset, waypointRightRightTail.gridY+1);
				if (rrWest != null)
                    rrWest.AddWaypoint(waypointRightRightTail, 2);
				
				Waypoint llWest = GridManager.GetWaypoint(waypointLeftLeftHead.gridX - xWaypointOffset, waypointLeftLeftHead.gridY-1);
				if (llWest != null)
                    waypointLeftLeftHead.AddWaypoint(llWest, 2);
			}

			IGridObject west = GridManager.GetGridObject(GridX - GridWidth, GridY);
			if (west != null && west.GridX == GridX - GridWidth && west.GridY == GridY && (west is StraightDoubleRoadRotatedEastward))
			{
				Waypoint rlWest = GridManager.GetWaypoint(waypointRightLeftTail.gridX - xWaypointOffset, waypointRightLeftTail.gridY);
				if (rlWest != null)
                    rlWest.AddWaypoint(waypointRightLeftTail, 1);
		
				Waypoint lrWest = GridManager.GetWaypoint(waypointLeftRightHead.gridX - xWaypointOffset, waypointLeftRightHead.gridY);
				if (lrWest != null)
                    waypointLeftRightHead.AddWaypoint(lrWest, 1);
			
				Waypoint rrWest = GridManager.GetWaypoint(waypointRightRightTail.gridX - xWaypointOffset, waypointRightRightTail.gridY);
				if (rrWest != null)
                    rrWest.AddWaypoint(waypointRightRightTail, 1);

				Waypoint llWest = GridManager.GetWaypoint(waypointLeftLeftHead.gridX - xWaypointOffset, waypointLeftLeftHead.gridY);
				if (llWest != null)
                    waypointLeftLeftHead.AddWaypoint(llWest, 1);
				
				Waypoint llWest2 = GridManager.GetWaypoint(waypointLeftLeftHead.gridX - xWaypointOffset, waypointLeftLeftHead.gridY-1);
				if (llWest2 != null)
                    waypointLeftLeftHead.AddWaypoint(llWest2, 2);

				Waypoint rlWest2 = GridManager.GetWaypoint(waypointRightLeftTail.gridX - xWaypointOffset, waypointRightLeftTail.gridY-1);
				if (rlWest2 != null)
                    rlWest2.AddWaypoint(waypointRightLeftTail, 2);

				Waypoint rrWest2 = GridManager.GetWaypoint(waypointRightRightTail.gridX - xWaypointOffset, waypointRightRightTail.gridY+1);
				if (rrWest2 != null)
                    rrWest2.AddWaypoint(waypointRightRightTail, 2);

				Waypoint lrWest2 = GridManager.GetWaypoint(waypointLeftRightHead.gridX - xWaypointOffset, waypointLeftRightHead.gridY+1);
				if (lrWest2 != null)
                    waypointLeftRightHead.AddWaypoint(lrWest2, 2);

			}
		
		}
	}
}
