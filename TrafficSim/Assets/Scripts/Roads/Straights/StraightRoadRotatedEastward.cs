﻿using UnityEngine;
using System.Collections;

public class StraightRoadRotatedEastward : StraightSingleRoad 
{
    

	override public void UpdateGrid()
	{
		int[] coordinates = GridManager.GetGridCoordinates(this.transform.position);
		GridX = coordinates[0];
		GridY = coordinates[1];

		if (GridManager.IsSpotVacant(this))
		{
			GridManager.PlaceObjectOnTheGrid(this);
			waypointLeftHead.UpdateGridCoordinates();
			waypointLeftTail.UpdateGridCoordinates();
			waypointRightHead.UpdateGridCoordinates();
			waypointRightTail.UpdateGridCoordinates();
			
			
			int xWaypointOffset = GridWidth / 2;
			
			
			IGridObject east = GridManager.GetGridObject(GridX + GridWidth, GridY);
			if (east != null && east.GridX == GridX + GridWidth && east.GridY == GridY && (east is StraightRoadRotatedEastward || east is Crossing4 || east is SingleTurnRight || east is SingleTurnRight1))
			{
				Waypoint rEast = GridManager.GetWaypoint(waypointRightHead.gridX + xWaypointOffset, waypointRightHead.gridY);
				if (rEast != null)
                    waypointRightHead.AddWaypoint(rEast, 1);
				
				Waypoint lEast = GridManager.GetWaypoint(waypointLeftTail.gridX + xWaypointOffset, waypointLeftTail.gridY);
				if (lEast != null)
                    lEast.AddWaypoint(waypointLeftTail, 1);
			}

			IGridObject eastDouble = GridManager.GetGridObject(GridX + GridWidth, GridY-1);
			if (eastDouble != null && eastDouble.GridX == GridX + GridWidth && eastDouble.GridY == GridY-1 && (eastDouble is StraightDoubleRoadRotatedEastward))
			{
				Waypoint rEast = GridManager.GetWaypoint(waypointRightHead.gridX + xWaypointOffset, waypointRightHead.gridY);
				if (rEast != null)
                    waypointRightHead.AddWaypoint(rEast, 1);

                Waypoint lEast = GridManager.GetWaypoint(waypointLeftTail.gridX + xWaypointOffset, waypointLeftTail.gridY);
                if (lEast != null)
                    lEast.AddWaypoint(waypointLeftTail, 1);

				Waypoint rEast2 = GridManager.GetWaypoint(waypointRightHead.gridX + xWaypointOffset, waypointRightHead.gridY-1);
				if (rEast2 != null)
					waypointRightHead.AddWaypoint(rEast2, 2);

				Waypoint lEast2 = GridManager.GetWaypoint(waypointLeftTail.gridX + xWaypointOffset, waypointLeftTail.gridY+1);
				if (lEast2 != null)
                    lEast2.AddWaypoint(waypointLeftTail, 2);
			}

			IGridObject west = GridManager.GetGridObject(GridX - GridWidth, GridY);
			if (west != null && west.GridX == GridX - GridWidth && west.GridY == GridY && ( west is StraightRoadRotatedEastward || west is Crossing4 || west is SingleTurnLeft || west is SingleTurnLeft1))
			{
				Waypoint rWest = GridManager.GetWaypoint(waypointRightTail.gridX - xWaypointOffset, waypointRightTail.gridY);
				if (rWest != null)
					rWest.AddWaypoint(waypointRightTail, 1);
				
				Waypoint lWest = GridManager.GetWaypoint(waypointLeftHead.gridX - xWaypointOffset, waypointLeftHead.gridY);
				if (lWest != null)
                    waypointLeftHead.AddWaypoint(lWest, 1);
			}

			IGridObject westDouble = GridManager.GetGridObject(GridX - 2, GridY-1);
			if (westDouble != null && westDouble.GridX == GridX - 2 && westDouble.GridY == GridY -1 && ( westDouble is StraightDoubleRoadRotatedEastward))
			{
				Waypoint rWest = GridManager.GetWaypoint(waypointRightTail.gridX - xWaypointOffset, waypointRightTail.gridY);
				if (rWest != null)
                    rWest.AddWaypoint(waypointRightTail, 1);

                Waypoint lWest = GridManager.GetWaypoint(waypointLeftHead.gridX - xWaypointOffset, waypointLeftHead.gridY);
                if (lWest != null)
                    waypointLeftHead.AddWaypoint(lWest, 1);

				Waypoint rWest2 = GridManager.GetWaypoint(waypointRightTail.gridX - xWaypointOffset, waypointRightTail.gridY-1);
				if (rWest2 != null)
					rWest2.AddWaypoint(waypointRightTail, 2);


				Waypoint lWest2 = GridManager.GetWaypoint(waypointLeftHead.gridX - xWaypointOffset, waypointLeftHead.gridY+1);
				if (lWest2 != null)
                    waypointLeftHead.AddWaypoint(lWest2, 2);
			}
		}
	}
}
