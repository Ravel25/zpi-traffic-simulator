﻿using UnityEngine;
using System.Collections;

public class StraightRoadRotatedNorthward : StraightSingleRoad 
{
	override public void UpdateGrid()
	{
		int[] coordinates = GridManager.GetGridCoordinates(this.transform.position);
		GridX = coordinates[0];
		GridY = coordinates[1];

		if (GridManager.IsSpotVacant(this))
		{
			GridManager.PlaceObjectOnTheGrid(this);
			waypointLeftHead.UpdateGridCoordinates();
			waypointLeftTail.UpdateGridCoordinates();
			waypointRightHead.UpdateGridCoordinates();
			waypointRightTail.UpdateGridCoordinates();
			
			int yWaypointOffset = GridHeight / 2;
			
			
			IGridObject north = GridManager.GetGridObject(GridX, GridY + GridHeight);
			if (north != null && north.GridX == GridX && north.GridY == GridY + GridHeight && (north is StraightRoadRotatedNorthward || north is Crossing4 || north is StraightDoubleRoadRotatedNorthward || north is SingleTurnLeft1 || north is SingleTurnRight))
			{
				Waypoint rNorth = GridManager.GetWaypoint(waypointRightHead.gridX, waypointRightHead.gridY + yWaypointOffset);
				if (rNorth != null)
					waypointRightHead.AddWaypoint(rNorth, 1);
				
				Waypoint lNorth = GridManager.GetWaypoint(waypointLeftTail.gridX, waypointLeftTail.gridY + yWaypointOffset);
				if (lNorth != null)
                    lNorth.AddWaypoint(waypointLeftTail, 1);
			}
			IGridObject northDouble = GridManager.GetGridObject(GridX-1, GridY + GridHeight);
			if (northDouble != null && northDouble.GridX == GridX-1 && northDouble.GridY == GridY + GridHeight && (northDouble is StraightDoubleRoadRotatedNorthward))
			{
				Waypoint rNorth = GridManager.GetWaypoint(waypointRightHead.gridX, waypointRightHead.gridY + yWaypointOffset);
				if (rNorth != null)
                    waypointRightHead.AddWaypoint(rNorth, 1);
				
				Waypoint lNorth = GridManager.GetWaypoint(waypointLeftTail.gridX, waypointLeftTail.gridY + yWaypointOffset);
				if (lNorth != null)
                    lNorth.AddWaypoint(waypointLeftTail, 1);

				Waypoint rNorth2 = GridManager.GetWaypoint(waypointRightHead.gridX+1, waypointRightHead.gridY + yWaypointOffset);
				if (rNorth2 != null)
                    waypointRightHead.AddWaypoint(rNorth2, 2);
				
				Waypoint lNorth2 = GridManager.GetWaypoint(waypointLeftTail.gridX-1, waypointLeftTail.gridY + yWaypointOffset);
				if (lNorth2 != null)
                    lNorth2.AddWaypoint(waypointLeftTail, 2);
			}
		IGridObject south = GridManager.GetGridObject(GridX, GridY - GridHeight);
			if (south != null && south.GridX == GridX && south.GridY == GridY - GridHeight && (south is StraightRoadRotatedNorthward || south is Crossing4 || south is SingleTurnLeft  || south is SingleTurnRight1))
			{
				Waypoint rSouth = GridManager.GetWaypoint(waypointRightTail.gridX, waypointRightTail.gridY - yWaypointOffset);
				if (rSouth != null)
                    rSouth.AddWaypoint(waypointRightTail, 1);
				
				Waypoint lSouth = GridManager.GetWaypoint(waypointLeftHead.gridX, waypointLeftHead.gridY - yWaypointOffset);
				if (lSouth != null)
					waypointLeftHead.AddWaypoint(lSouth, 1);
			}
			IGridObject southDouble = GridManager.GetGridObject(GridX-1, GridY - GridHeight);
			if (southDouble != null && southDouble.GridX == GridX-1 && southDouble.GridY == GridY - GridHeight && (southDouble is StraightDoubleRoadRotatedNorthward))
			{
				Waypoint rSouth = GridManager.GetWaypoint(waypointRightTail.gridX, waypointRightTail.gridY - yWaypointOffset);
				if (rSouth != null)
					rSouth.AddWaypoint(waypointRightTail, 1);

				Waypoint lSouth = GridManager.GetWaypoint(waypointLeftHead.gridX, waypointLeftHead.gridY - yWaypointOffset);
				if (lSouth != null)
                    waypointLeftHead.AddWaypoint(lSouth, 1);

                Waypoint rSouth2 = GridManager.GetWaypoint(waypointRightTail.gridX + 1, waypointRightTail.gridY - yWaypointOffset);
                if (rSouth2 != null)
                    rSouth2.AddWaypoint(waypointRightTail, 2);

				Waypoint lSouth2 = GridManager.GetWaypoint(waypointLeftHead.gridX-1, waypointLeftHead.gridY - yWaypointOffset);
				if (lSouth2 != null)
					waypointLeftHead.AddWaypoint(lSouth2, 2);
			}
		}
	}
}
