﻿using UnityEngine;
using System.Collections;
	
public abstract class StraightSingleRoad : Road 
{
		
	public Waypoint waypointLeftHead,
        waypointLeftTail,
	    waypointRightHead,
        waypointRightTail;
		
	public override void DrawGizmos(Color color)
	{
		waypointLeftHead.DrawGizmos(color);
		waypointLeftTail.DrawGizmos(color);
		waypointRightHead.DrawGizmos(color);
		waypointRightTail.DrawGizmos(color);
	}
}

