﻿using UnityEngine;
using System.Collections.Generic;

public class Roads : MonoBehaviour
{
    public bool debugModeEnabled = true;
    public Color debugColor = Color.green;
    private Dictionary<int, Road> roads;
    private int _lastId;

    void Reset()
    {
        UpdateRoads();
    }

    public void UpdateRoads()
    {
        _lastId = 0;
        Road[] roadsArray = GetComponentsInChildren<Road>();
        roads = new Dictionary<int, Road>();
        for (int i = 0; i < roadsArray.Length; i++)
        {
            roadsArray[i].Id = _lastId;
            roads.Add(roadsArray[i].Id, roadsArray[i]);
            _lastId++;
        }
    }

    void OnDrawGizmos()
    {
        if (roads == null)
            UpdateRoads();

        if (debugModeEnabled && roads != null)
        {
            //for (int i = 0; i < roads.Length; i++)
            //{
            //    roads[i].DrawGizmos(debugColor);
            //}

            foreach (Road road in roads.Values)
            {
                if (road != null)
                {
                    road.DrawGizmos(debugColor);
                }
            }
        }
    }

    public void AddRoad(ref Road road)
    {
        _lastId ++;
        road.Id = _lastId;
        roads.Add(road.Id, road);
    }

    public void RemoveRoad(int id)
    {
        Debug.Log("removing    " +  roads.Count);
        roads.Remove(id);
        Debug.Log("removed    " + roads.Count);
    }
}
