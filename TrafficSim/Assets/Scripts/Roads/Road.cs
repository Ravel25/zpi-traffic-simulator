﻿using UnityEngine;
using System.Collections;
using System;


public abstract class Road : MonoBehaviour, IGridObject
{
    public abstract void DrawGizmos(Color color);

    public int GridX { get; set; }
    public int GridY { get; set; }
    public int GridWidth
    {
        get { return 2; }
    }
    public int GridHeight
    {
        get { return 2; }
    }

    public abstract void UpdateGrid();
    public int Id;

    //public void OnDestroy()
    //{
    //    Debug.Log("Destroy road");
      
    //}

    

}
