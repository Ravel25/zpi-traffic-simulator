﻿using UnityEngine;
using System.Collections;
using System;



public class GridManager : MonoBehaviour
{
    public Terrain terrain;
    public static int gridScale = 2;
    public bool debugModeEnabled = true;
    public Color gridColor = Color.cyan;    
    
    
    private static IGridObject[,] gridObjects;
    private static Waypoint[,] waypoints;
    private static Vector3 worldOrigin;
    private static int mapWidth, mapHeight;

    void Awake()
    {
        worldOrigin = terrain.GetPosition();
        mapWidth = (int) (terrain.terrainData.size.x / gridScale);
        mapHeight = (int) (terrain.terrainData.size.z / gridScale);

        gridObjects = new IGridObject[mapWidth, mapHeight];
        waypoints = new Waypoint[mapWidth, mapHeight];

        Road[] roadsArray = GetComponentsInChildren<Road>();
        Roads roads = GetComponentInChildren<Roads>();
        roads.UpdateRoads();
        foreach (Road road in roadsArray)
        {
            road.UpdateGrid();
        }
        
    }


    private void OnDrawGizmos()
    {
        if (debugModeEnabled)
        {
            Vector3 worldOrigin = terrain.GetPosition();
            float width = terrain.terrainData.size.x;
            float height = terrain.terrainData.size.z;
            Vector3 start = worldOrigin;
            Vector3 end = worldOrigin;

            start.y = worldOrigin.y + 0.01f;
            end.y = worldOrigin.y + 0.01f;
            end.z += height;

            while (start.x <= worldOrigin.x + width)
            {
                Debug.DrawLine(start, end, gridColor);

                start.x += gridScale;
                end.x += gridScale;
            }

            start.x = worldOrigin.x;
            end.x = worldOrigin.x + width;
            end.z = worldOrigin.z;

            while (start.z <= worldOrigin.z + height)
            {
                Debug.DrawLine(start, end, gridColor);

                start.z += gridScale;
                end.z += gridScale;
            }
        }
    }

    public static int[] GetGridCoordinates(Vector3 position)
    {
        return new int[]{
            (int) ((position.x - worldOrigin.x) / gridScale),
            (int) ((position.z - worldOrigin.z) / gridScale)};
    }

    public static void PlaceObjectOnTheGrid(IGridObject gridObject)
    {
        for (int i = 0; i < gridObject.GridWidth; i++)
            for (int j = 0; j < gridObject.GridHeight; j++)
                gridObjects[gridObject.GridX + i, gridObject.GridY + j] = gridObject;
    }

    public static bool IsSpotVacant(IGridObject gridObject)
    {
        for (int i = 0; i < gridObject.GridWidth; i++)
            for (int j = 0; j < gridObject.GridHeight; j++)
            {
                if (gridObjects[gridObject.GridX + i, gridObject.GridY + j] != null)
                    return false;
            }
        return true;
    }

    private static bool AreCoordinatesValid(int gridX, int gridY)
    {
        return gridX >= 0 && gridX < mapWidth &&
               gridY >= 0 && gridY < mapHeight;
    }

    public static IGridObject GetGridObject(int gridX, int gridY)
    {
        return AreCoordinatesValid(gridX, gridY) ? gridObjects[gridX, gridY] : null;
    }

    public static void PlaceWaypointOnTheGrid(Waypoint waypoint)
    {
        // consider procedure with explicit coordinates
        waypoints[waypoint.gridX, waypoint.gridY] = waypoint;
    }

    public static Waypoint GetWaypoint(int gridX, int gridY)
    {
        return AreCoordinatesValid(gridX, gridY) ? waypoints[gridX, gridY] : null;
    }
}
