﻿using UnityEngine;
using System.Collections.Generic;

public class PathsContainer : MonoBehaviour
{
    private Dictionary<int, Dictionary<int, Waypoint[]>> _pathsCache;

    void Start()
    {
        _pathsCache = new Dictionary<int, Dictionary<int, Waypoint[]>>();
    }

    public Waypoint[] TryGetPath(int originId, int goalId)
    {
        if (_pathsCache.ContainsKey(originId) && _pathsCache[originId].ContainsKey(goalId))
            return _pathsCache[originId][goalId];
        else
            return null;
    }

    public void AddPath(int originId, int goalId, Waypoint[] path)
    {
        if( !_pathsCache.ContainsKey(originId))
            _pathsCache.Add(originId, new Dictionary<int,Waypoint[]>());

        _pathsCache[originId].Add(goalId, path);
    }
}
