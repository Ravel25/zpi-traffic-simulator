﻿using UnityEngine;
using System.Collections.Generic;

public class VehiclesContainer : MonoBehaviour
{
    public static HashSet<VehicleMemory> vehicles;


    void Awake()
    {
        vehicles = new HashSet<VehicleMemory>();
    }

    public void AddVehicle(ref VehicleMemory vehicle)
    {
        vehicles.Add(vehicle);
    }

    public void RemoveVehicle(VehicleMemory vehicle)
    {
        vehicles.Remove(vehicle);
    }

}
