﻿using UnityEngine;
using System.Collections;
using RAIN.Core;
using RAIN.Serialization;

[RAINSerializableClass]
public class VehicleMemoryAIExtension : CustomAIElement 
{
    [RAINSerializableField]
    public VehicleMemory memory;

    [RAINSerializableField]
    public VehicleSight sight;

    public override void AIInit()
    {
        base.AIInit();
    }
}
