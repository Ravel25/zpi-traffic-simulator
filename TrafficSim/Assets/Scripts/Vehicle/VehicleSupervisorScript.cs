﻿using UnityEngine;
using System.Collections;

public class VehicleSupervisorScript : MonoBehaviour
{
    
    //public RAIN.Core.AIRig aiRig;
    private VehicleMemory _memory;
    private Transform _cachedTransform;

    void Start()
    {
        _memory = GetComponent<VehicleMemory>();
        _cachedTransform = _memory.cachedComponents.transform;
    }

    void OnDrawGizmos()
    {
        if( _memory != null && _memory.speed > 0)
        {
            DrawArrow.ForDebug(_cachedTransform.position, _cachedTransform.forward * _memory.speed, 1);
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.GetComponent<VehicleSupervisorScript>())
        {
            PerformAccident();
        }
    }

    void OnTriggerExit(Collider collider)
    {
    }


    void PerformAccident()
    {
        _memory.accidentOccured = true;
        _memory.speed = 0;
        _memory.acceleration = 0;
        rigidbody.isKinematic = false;
        this.collider.isTrigger = false;
        //DestroyObject(this.gameObject, _memory.accidentDuration);
    }
}
