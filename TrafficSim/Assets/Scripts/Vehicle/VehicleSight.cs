﻿using UnityEngine;
using System.Collections.Generic;

public class VehicleSight : MonoBehaviour
{
    public float horizontalFOV = 120f,
                 verticalFOV = 90f,
                 sightRange = 15f,
                 sightRefreshRate = 0.2f,
                 extensiveSightRange = 10f,
                 extensiveSightRefreshRate = 0.2f,
                 backSightRefreshRate = 0.2f,
                 smellRange = 5f,
                 smellRefreshRate = 0.2f,
                 backSightMinHorizontalAngle = 40f;
    public Color sightGizmoColor, smellGizmoColor;

    public bool runExtensiveSight,
                runSmell,
                runBackSight,
                runSingleLaneCrossingAwareness,
                runDoubleLaneCrossingAwareness,
                runLaneChangeAwareness;
    public Transform leftSensorPosition, rightSensorPosition;

    private float _maxHorizontalAngle, _maxVerticalAngle, _sightReuseTime, _extensiveSightReuseTime, _smellReuseTime, _backSightReuseTime;
    private int _vehiclesLayerMask, _trafficLightsLayerMask, _crossingsLayerMask, _gridX, _gridY;
    private VehicleMemory _memory;
    private Transform _cachedTransform;


	void Start() 
    {
        _memory = GetComponent<VehicleMemory>();
        _cachedTransform = _memory.cachedComponents.transform;

        _maxHorizontalAngle = horizontalFOV / 2.0f;
        _maxVerticalAngle = verticalFOV / 2.0f;
        _vehiclesLayerMask = LayerMask.GetMask("Vehicles");
        _trafficLightsLayerMask = LayerMask.GetMask("Traffic lights");
        _crossingsLayerMask = LayerMask.GetMask("Crossings");
    }
	
	void Update() 
    {
        _sightReuseTime += Time.deltaTime;
        if (_sightReuseTime > sightRefreshRate)
        {
            _sightReuseTime = 0;
            RunSimpleSight();

            if (_memory.isTurnAhead)
            {
                VehicleMemory otherVehicle = PathCheckForVehicles();
                if (otherVehicle != null)
                {
                    float distance = Vector3.Distance(_cachedTransform.position, otherVehicle.cachedComponents.transform.position);

                    if (_memory.vehicleInFront && distance < _memory.frontVehicleDistance || !_memory.vehicleInFront)
                    {
                        _memory.vehicleInFront = true;
                        _memory.frontVehicleDistance = distance; //frontRaycastHit.distance;
                        _memory.frontVehicleSpeed = otherVehicle.speed;
                    }
                }
                else if (_memory.vehicleInFront)
                {
                    _memory.vehicleInFront = false;
                }
            }
        }

        _extensiveSightReuseTime += Time.deltaTime;
        if (runExtensiveSight && _extensiveSightReuseTime > extensiveSightRefreshRate)
        {
            _extensiveSightReuseTime = 0;
            RunExtensiveSight();


            if (runSingleLaneCrossingAwareness)
                RunSingleLaneCrossingAwareness();

            if (runDoubleLaneCrossingAwareness)
                RunDoubleLaneCrossingAwareness();
        }

        _smellReuseTime += Time.deltaTime;
        if (runSmell && _smellReuseTime > sightRefreshRate)
        {
            _smellReuseTime = 0;
            RunSmell();
        }

        _backSightReuseTime += Time.deltaTime;
        if (runBackSight && _backSightReuseTime > backSightRefreshRate)
        {
            _backSightReuseTime = 0;
            RunBackSight();

            if (runLaneChangeAwareness)
                RunLaneChangeAwareness();
        }   


	}


    void RunSimpleSight()
    {
        LookForTurn();
        LookForVehicleInFront();
        LookForTrafficLights();
        LookForCrossings();
        LookForReverse();
        LookForLaneChange();
    }


    void LookForVehicleInFront()
    {
        RaycastHit frontRaycastHit;
        // i guess we wont need it
        //if (Physics.Raycast(_cachedTransform.position, _cachedTransform.forward, out frontRaycastHit, sightRange, _vehiclesLayerMask))
        //{
        //    VehicleInFrontSpotted(frontRaycastHit);
        //}
        if (Physics.Raycast(leftSensorPosition.position, _cachedTransform.forward, out frontRaycastHit, sightRange, _vehiclesLayerMask))
        {
            VehicleInFrontSpotted(frontRaycastHit.collider.GetComponentInParent<VehicleMemory>(), frontRaycastHit.distance);
        }
        else if (Physics.Raycast(rightSensorPosition.position, _cachedTransform.forward, out frontRaycastHit, sightRange, _vehiclesLayerMask))
        {
            VehicleInFrontSpotted(frontRaycastHit.collider.GetComponentInParent<VehicleMemory>(), frontRaycastHit.distance);
        }
        else
        {
            _memory.vehicleInFront = false;
        }
    }

    void VehicleInFrontSpotted(VehicleMemory otherVehicle, float distance)// RaycastHit frontRaycastHit)
    {
        _memory.vehicleInFront = true;
        _memory.frontVehicleDistance = distance; //frontRaycastHit.distance;
        _memory.frontVehicleSpeed = otherVehicle.speed;
        _memory.vehicleInFrontTurning = otherVehicle.isTurnAhead;
        _memory.vehicleInFrontTurningLeft = otherVehicle.isTurnLeftAhead;
        _memory.vehicleInFrontOnCrossing = otherVehicle.isOnCrossing;
    }

    void LookForTrafficLights()
    {
        RaycastHit frontRaycastHit;
        if (Physics.Raycast(_cachedTransform.position, _cachedTransform.forward, out frontRaycastHit, sightRange, _trafficLightsLayerMask))
        {
            _memory.trafficLightsInFront = true;
            _memory.frontTrafficLightsDistance = frontRaycastHit.distance;
        }
        else
        {
            _memory.trafficLightsInFront = false;
        }
    }

    void LookForCrossings()
    {
        RaycastHit frontRaycastHit;
        if (Physics.Raycast(_cachedTransform.position, _cachedTransform.forward, out frontRaycastHit, sightRange, _crossingsLayerMask))
        {
            _memory.crossingInFront = true;
            _memory.crossingDistance = frontRaycastHit.distance;
        }
        else
        {
            _memory.crossingInFront = false;
        }
    }


    void RunBackSight()
    {
        _memory.visibleVehicles.Clear();
        Collider[] colliderHitArray = Physics.OverlapSphere(_cachedTransform.position, sightRange, _vehiclesLayerMask);
        foreach (Collider colliderHit in colliderHitArray)
        {
            if (IsVisibleInBack(colliderHit.transform.position))
            {
                VehicleMemory otherVehicle = colliderHit.gameObject.GetComponentInParent<VehicleMemory>();
                if (otherVehicle != null && _memory.instanceId != otherVehicle.instanceId)
                {
                    _memory.visibleVehicles.Add(otherVehicle);
                }
            }
        }
    }

    void RunExtensiveSight()
    {
        _memory.visibleVehicles.Clear();
        Collider[] colliderHitArray = Physics.OverlapSphere(_cachedTransform.position, sightRange, _vehiclesLayerMask);
        foreach (Collider colliderHit in colliderHitArray)
        {
            if (IsVisibleInBack(colliderHit.transform.position))
            {
                VehicleMemory otherVehicle = colliderHit.gameObject.GetComponentInParent<VehicleMemory>();
                if (otherVehicle != null && _memory.instanceId != otherVehicle.instanceId)
                {
                    _memory.visibleVehicles.Add(otherVehicle);
                }
            }
        } 
    }


    void RunSmell()
    {
        _memory.smelledVehicles.Clear();
        Collider[] colliderHitArray = Physics.OverlapSphere(_cachedTransform.position, smellRange, _vehiclesLayerMask);
        foreach (Collider colliderHit in colliderHitArray)
        {
            VehicleMemory otherVehicle = colliderHit.gameObject.GetComponentInParent<VehicleMemory>();
            if (otherVehicle != null && _memory.instanceId != otherVehicle.instanceId)
            {
                _memory.smelledVehicles.Add(otherVehicle);
            }
        }
    }


    bool IsTurnAhead()
    {
        int currentWaypointIndex = _memory._currentWaypointIndex - 1,
            gridX = _memory.path[currentWaypointIndex].gridX,
            gridY = _memory.path[currentWaypointIndex].gridY,
            dx = 0, dy = 0;

        for (int i = currentWaypointIndex; i < currentWaypointIndex + sightRange / GridManager.gridScale && i < _memory.path.Length; i++)
        {
            dx += _memory.path[i].gridX - gridX;
            dy += _memory.path[i].gridY - gridY;

            gridX = _memory.path[i].gridX;
            gridY = _memory.path[i].gridY;

            if (dx != 0 && dy != 0)
            {
                Vector3 vectorToWaypoint = _memory.path[i].cachedTransform.position - _memory.path[currentWaypointIndex].cachedTransform.position;
                Vector3 cross = Vector3.Cross(_cachedTransform.forward, vectorToWaypoint);

                _memory.isTurnLeftAhead = cross.y < 0;
                return true;
            }
        }
        return false;
    }


    void LookForTurn()
    {
        if (_memory.path != null && _memory.path.Length > 0 && _memory._currentWaypointIndex > 0)
        {
            if (!_memory.isOnCrossing || _memory.canLeaveCrossing)
                _memory.isTurnAhead = IsTurnAhead();
        }
        else
        {
            _memory.isTurnAhead = false;
            _memory.isTurnLeftAhead = false;
        }
    }

    bool IsReversing()
    {
        int checkingWaypointIndex = _memory._currentWaypointIndex + (int)(sightRange / GridManager.gridScale);

        if (checkingWaypointIndex < _memory.path.Length)
        {
            Vector3 foo = _memory.path[checkingWaypointIndex].cachedTransform.position - _memory.path[checkingWaypointIndex - 1].cachedTransform.position;
            float angle = Vector3.Angle(foo, _cachedTransform.forward);
            return angle > 170;

        }

        return false;
    }

    void LookForReverse()
    {
        if (_memory.path != null && _memory.path.Length > 0 && _memory._currentWaypointIndex > 0)
        {
            if (_memory.isOnCrossing && _memory.reversing && _memory.canLeaveCrossing && !_memory.isTurnAhead)
                _memory.reversing = false;

            else if(_memory.crossingInFront && !_memory.reversing)
                _memory.reversing = IsReversing();
        }
        else
        {
            _memory.reversing = false;
        }
    }


    bool IsLaneChangeAhead()
    {
        int currentWaypointIndex = _memory._currentWaypointIndex == 0 ? 0 : _memory._currentWaypointIndex - 1;
        bool leftLane = _memory.path[currentWaypointIndex].leftLane;

        for (int i = currentWaypointIndex + 1; i < currentWaypointIndex + sightRange / GridManager.gridScale && i < _memory.path.Length; i++)
        {
            int gridX = _memory.path[i].gridX,
                gridY = _memory.path[i].gridY;
            IGridObject gridObject = GridManager.GetGridObject(gridX, gridY);


            if (!(gridObject is DoubleCrossing4) && _memory.path[i].leftLane != leftLane)
            {
                _memory.laneChangeAwaitingWaypointIndex = i-1;
                return true;
            }
        }
        return false;
    }

    void LookForLaneChange()
    {
        if (_memory.path != null && _memory.path.Length > 0 && _memory._currentWaypointIndex > 0)
        {
            _memory.laneChangeAhead = IsLaneChangeAhead();
        }
        else
        {
            _memory.laneChangeAhead = false;
        }
    }


    VehicleMemory PathCheckForVehicles()
    {
        int currentWaypointIndex = _memory._currentWaypointIndex;

        for (int i = currentWaypointIndex; i < currentWaypointIndex + sightRange && i < _memory.path.Length; i++)
        {
            Collider[] colliderHitArray = Physics.OverlapSphere(_memory.path[i].cachedTransform.position, 1, _vehiclesLayerMask);
            float minDistance = float.MaxValue;
            VehicleMemory nearestVehicle = null;
            foreach (Collider hit in colliderHitArray)
            {

                VehicleMemory otherVehicle = hit.gameObject.GetComponentInParent<VehicleMemory>();
                if (otherVehicle != null && _memory.instanceId != otherVehicle.instanceId)
                {
                    float distance = Vector3.Distance(_cachedTransform.position, otherVehicle.cachedComponents.transform.position);
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        nearestVehicle = otherVehicle;
                    }
                }
            }

            if( nearestVehicle != null)
                return nearestVehicle;
        }

        return null;
    }




    bool IsVisibleInBack(Vector3 otherPosition)
    {
        Vector3 toOther = otherPosition - _cachedTransform.position;
        Vector3 horizontalVector = toOther - Vector3.Project(toOther, _cachedTransform.up);

        float horizontalAngle = Vector3.Angle(horizontalVector, _cachedTransform.forward);
              //verticalAngle = Vector3.Angle(toOther, horizontalVector);

        return horizontalAngle > backSightMinHorizontalAngle;
    }

    bool IsVisible(Vector3 otherPosition)
    {
        Vector3 toOther = otherPosition - _cachedTransform.position;
        Vector3 horizontalVector = toOther - Vector3.Project(toOther, _cachedTransform.up);

        float horizontalAngle = Vector3.Angle(horizontalVector, _cachedTransform.forward),
              verticalAngle = Vector3.Angle(toOther, horizontalVector);

        return horizontalAngle < _maxHorizontalAngle && verticalAngle < _maxVerticalAngle;
    }

    void RunSingleLaneCrossingAwareness()
    {
        bool canLeaveCrossing = CanLeaveCrossing();
        if (canLeaveCrossing)
        {
            _memory.arrivingDistance = 0;
            runExtensiveSight = runSmell = runSingleLaneCrossingAwareness = false;
        }
        _memory.canLeaveCrossing = canLeaveCrossing;
    }

    bool CanLeaveCrossing()
    {
        foreach (VehicleMemory vehicle in _memory.visibleVehicles)
        {
            float angle = Vector3.Angle(_memory.cachedComponents.transform.forward, vehicle.cachedComponents.transform.forward);
            if (angle > 160)
            {
                float timeToCrossing = 0;
                float distance = (vehicle.cachedComponents.transform.position - _memory.cachedComponents.transform.position).magnitude;
                if (vehicle.speed == 0)
                {
                    if (vehicle.trafficLightsInFront)
                    {
                        timeToCrossing = float.MaxValue;
                    }
                    else
                    {
                        timeToCrossing = 0;
                    }
                }
                else
                {
                    if (vehicle.trafficLightsInFront && vehicle.crossingInFront && vehicle.crossingDistance > 3f)
                    {
                        timeToCrossing = float.MaxValue;
                    }
                    else
                    {
                        //timeToCrossing = distance / vehicle.maxSpeed;
                        timeToCrossing = (-vehicle.speed + Mathf.Sqrt(vehicle.speed * vehicle.speed + 2 * vehicle.acceleration * distance)) / vehicle.maxAcceleration + 2;

                    }
                }


                if (_memory.timeToLeaveCrossing > timeToCrossing)
                    return false;
            }
        }

        return true;
    }
    

    void RunDoubleLaneCrossingAwareness()
    {
        bool canLeaveCrossing = CanLeaveDoubleLaneCrossing();
        if (canLeaveCrossing)
        {
            _memory.arrivingDistance = 0;
            runExtensiveSight = runSmell = runDoubleLaneCrossingAwareness = false;
        }
        _memory.canLeaveCrossing = canLeaveCrossing;
    }


    bool CanChangeLane()
    {
        if (_memory.vehicleInFront && _memory.frontVehicleDistance < _memory.safeDistance + 3)
            return false;

        foreach (VehicleMemory vehicle in _memory.visibleVehicles)
        {
            float angle = Vector3.Angle(_memory.cachedComponents.transform.forward, vehicle.cachedComponents.transform.forward);
            if (angle < 60)
            {
                float distance = (vehicle.cachedComponents.transform.position - _memory.cachedComponents.transform.position).magnitude + 3;
                float timeToCollisionPoint = (-vehicle.speed + Mathf.Sqrt(vehicle.speed * vehicle.speed + 2 * vehicle.acceleration * distance) ) / vehicle.maxAcceleration;

                if (_memory.timeToChangeLane > timeToCollisionPoint)
                    return false;
                
            }
        }
        return true;
    }

    void RunLaneChangeAwareness()
    {
        bool canChangeLane = CanChangeLane();
        if (canChangeLane)
        {
            _memory.arrivingDistance = 0;
            runBackSight = runLaneChangeAwareness = _memory.laneChangeAhead = false;
        }

        _memory.canChangeLane = canChangeLane;
    }

    

    bool CanLeaveDoubleLaneCrossing()
    {
        return false;
    }

    private VehicleMemory GetNearestVehicle()
    {
        VehicleMemory nearestVehicle = null;
        float lowestDistanceSqr = float.MaxValue;

        foreach (VehicleMemory vehicle in _memory.visibleVehicles)
        {
            float distanceSqr = (vehicle.cachedComponents.transform.position - _memory.cachedComponents.transform.position).sqrMagnitude;
            if (distanceSqr < lowestDistanceSqr)
            {
                nearestVehicle = vehicle;
                lowestDistanceSqr = distanceSqr;
            }
        }

        return nearestVehicle;
    }

    void OnDrawGizmosSelected()
    {
        if (_cachedTransform == null)
        {
            Gizmos.color = sightGizmoColor;
            Gizmos.DrawWireSphere(this.transform.position, sightRange);
            Gizmos.color = smellGizmoColor;
            Gizmos.DrawWireSphere(this.transform.position, smellRange);
        }
        else
        {
            Gizmos.color = sightGizmoColor;
            Gizmos.DrawWireSphere(_cachedTransform.position, sightRange);
            Gizmos.color = smellGizmoColor;
            Gizmos.DrawWireSphere(_cachedTransform.position, smellRange);
        }
    }










    //*****************************************************//
    //                                                     //
    //                    DEPRECATED                       //
    //                                                     //
    //*****************************************************//

    // keep it just in case ;)

    //bool IsOnCrossing()
    //{
    //    IGridObject gridObject = GridManager.GetGridObject(_gridX, _gridY);
    //    return gridObject is Crossing4 || gridObject is DoubleCrossing4;
    //}

    //void UpdateGridCoordinates()
    //{
    //    _gridX = (int)_cachedTransform.position.x / GridManager.gridScale;
    //    _gridY = (int)_cachedTransform.position.z / GridManager.gridScale;
    //}
}
