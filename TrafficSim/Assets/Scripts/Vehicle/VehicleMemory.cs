﻿using UnityEngine;
using System.Collections.Generic;

public class VehicleMemory : MonoBehaviour
{
    public VehiclesContainer vehiclesContainer;
    public PathsContainer pathsContainer;
    public Waypoint[] path;
    public Waypoint start, goal;

    public int _currentWaypointIndex, instanceId, crossingInitialWaypointIndex, crossingWaypointsAdvance, changePoint, laneChangeAwaitingWaypointIndex;
    public bool recalculatePath,
                vehicleInFront,
                vehicleInFrontTurning,
                vehicleInFrontTurningLeft,
                vehicleInFrontOnCrossing,
                trafficLightsInFront,
                accidentOccured,
                isTurnAhead,
                isTurnLeftAhead,
                isOnCrossing,
                crossingInFront,
                canLeaveCrossing,
                exploded,
                speedLimitEnabled,
                isOnDoubleLaneCrossing,
                reversing,
                laneChangeAhead,
                canChangeLane;



    public float waypointMinAcceptanceDistanceSqr,
                 waypointMinAcceptanceDistanceSqrOnCrossing,
                 safeDistance,
                 safeDistanceSqr,
                 frontVehicleSpeed,
                 frontVehicleDistance,
                 frontTrafficLightsDistance,
                 explosionDelay,
                 accidentDuration,
                 maxAcceleration,
                 maxDeceleration,
                 maxSpeed,
                 maxTurnSpeed,
                 timeToLeaveCrossing,
                 timeToLeaveDoubleCrossing,
                 timeToChangeLane,
                 arrivingDistance,
                 minDistanceToStop,
                 crossingDistance,
                 timeSinceAccident,
                 speedLimit,
                 maxRotationSpeed,
                 updatePositionLastTime;

    public GameObject explosion;

    public float speed, acceleration;
    public Vector3 orientation;


    public struct CachedComponents
    {
        public Transform transform;
        public VehicleSpecs vehicleSpecs;
        public VehicleDrivingStyle drivingStyle;
    }
    public CachedComponents cachedComponents;
    public HashSet<VehicleMemory> visibleVehicles;
    public HashSet<VehicleMemory> smelledVehicles;


    void Awake()
    {
        instanceId = GetInstanceID();
        path = null;
        cachedComponents = new CachedComponents() { 
            transform = this.transform,
            vehicleSpecs = GetComponent<VehicleSpecs>(),
            drivingStyle = GetComponent<VehicleDrivingStyle>()
        };
        orientation = cachedComponents.transform.rotation.eulerAngles;

        maxAcceleration = cachedComponents.vehicleSpecs.maxAcceleration * cachedComponents.drivingStyle.accelerationFactor;
        maxDeceleration = cachedComponents.vehicleSpecs.maxDeceleration * cachedComponents.drivingStyle.decelerationFactor;
        maxSpeed = cachedComponents.vehicleSpecs.maxSpeed * cachedComponents.drivingStyle.speedFactor;
        maxTurnSpeed = maxSpeed * cachedComponents.drivingStyle.turnSlowdown;
        maxRotationSpeed = cachedComponents.drivingStyle.rotationFactor * cachedComponents.vehicleSpecs.rotationSpeed;
        safeDistanceSqr = safeDistance * safeDistance;
        minDistanceToStop = -maxSpeed * maxSpeed / 2 / cachedComponents.vehicleSpecs.maxDeceleration;
        visibleVehicles = new HashSet<VehicleMemory>();
        smelledVehicles = new HashSet<VehicleMemory>();

        float timeToSpeedUpOnCrossing = maxTurnSpeed / maxAcceleration;
        float roadAtThatTime = maxAcceleration * timeToSpeedUpOnCrossing * timeToSpeedUpOnCrossing / 2;
        if (roadAtThatTime < 8)
        {
            float timeFoo = (8 - roadAtThatTime) / maxTurnSpeed;
            timeToSpeedUpOnCrossing += timeFoo;
        }
        
        //timeToLeaveCrossing = Mathf.Sqrt( 2 * 5 / maxAcceleration);
        timeToLeaveCrossing = timeToSpeedUpOnCrossing + 2;


        float timeToSpeedUpOnLaneChange = maxTurnSpeed / maxAcceleration;
        float roadAtThatTime2 = maxAcceleration * timeToSpeedUpOnLaneChange * timeToSpeedUpOnLaneChange / 2;
        if (roadAtThatTime2 < 7)
        {
            float timeFoo = (7 - roadAtThatTime2) / maxTurnSpeed;
            timeToSpeedUpOnLaneChange += timeFoo;
        }

        timeToChangeLane = timeToSpeedUpOnLaneChange + 2;
    }


    public bool HasArrived(Vector3 position)
    {
        return (_currentWaypointIndex == path.Length - 1) && IsWaypointAccepted(position);
    }

    public bool IsWaypointAccepted(Vector3 position)
    {
        return (position - path[_currentWaypointIndex].cachedTransform.position).sqrMagnitude < waypointMinAcceptanceDistanceSqr;
    }

    public bool IsWaypointAcceptedOnCrossing(Vector3 position)
    {
        return (position - path[_currentWaypointIndex].cachedTransform.position).sqrMagnitude < waypointMinAcceptanceDistanceSqrOnCrossing;
    }

    public void MoveToNextWaypoint()
    {
        _currentWaypointIndex++;
    }

    public Waypoint GetCurrentWaypoint()
    {
        if (_currentWaypointIndex >= 0 && _currentWaypointIndex < path.Length)
            return path[_currentWaypointIndex];
        else
            return null;
    }

    public void DestroyVehicle()
    {
        DestroyObject(this.gameObject);
    }

    public void Explode()
    {
        Instantiate(explosion, cachedComponents.transform.position, Quaternion.identity);
        DestroyObject(this.gameObject, accidentDuration - explosionDelay);
        BroadcastMessage("ChangeColorToBurnt");
    }

    void OnDestroy()
    {
        vehiclesContainer.RemoveVehicle(this);
    }
}
