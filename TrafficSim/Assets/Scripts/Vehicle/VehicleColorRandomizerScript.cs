﻿using UnityEngine;
using System.Collections;

public class VehicleColorRandomizerScript : MonoBehaviour
{
    void Start()
    {
        renderer.material.color = new Color(
            Random.Range(0.1f, 0.73f),
            Random.Range(0.1f, 0.73f),
            Random.Range(0.1f, 0.73f));
    }

    public void ChangeColorToBurnt()
    {
        renderer.material.color = new Color(0.1f, 0.1f, 0.1f);
    }


}
