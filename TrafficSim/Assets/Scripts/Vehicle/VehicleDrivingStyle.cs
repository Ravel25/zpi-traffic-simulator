﻿using UnityEngine;
using System.Collections;

public class VehicleDrivingStyle : MonoBehaviour 
{
    // values range between 0..1
    public float accelerationFactor = 0.5f,
                 decelerationFactor = 1f,
                 speedFactor = 0.5f,
                 rotationFactor = 1f,
                 turnSlowdown = 0.5f;


}
