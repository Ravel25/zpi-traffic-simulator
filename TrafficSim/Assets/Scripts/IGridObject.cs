﻿using UnityEngine;
using System.Collections;
using System;


public interface IGridObject
{
    int GridX { get; set; }
    int GridY { get; set; }
    int GridWidth { get;}
    int GridHeight { get;}
}
