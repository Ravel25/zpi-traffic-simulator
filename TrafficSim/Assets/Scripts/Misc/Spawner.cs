﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
    public VehiclesContainer vehiclesContainer;
    public PathsContainer pathsContainer;
    public Waypoint[] origins, 
        goals;
    public GameObject[] vehiclePrefabs;
    public GameObject originMarker, goalMarker;
    public float[] rouletteSelection;
    public float spawnInterval,
        collisionCheckRadius,
        heightOverWaypoint;
    public bool singleSpawn;

    private Transform _cachedVehicleParent;
    private float _timePassedBy,
        _spawnInterval;
    private int _sightLayerMask;
    
    

    void Awake()
    {
        _cachedVehicleParent = vehiclesContainer.transform;
        _sightLayerMask = LayerMask.GetMask("Vehicles"); 
    }

    void Start()
    {
        PlaceMarkers();
        //GenerateVehicle();
        UpdateSpawnInterval();
        _timePassedBy = 0;
    }


    void PlaceMarkers()
    {
        foreach (Waypoint origin in origins)
        {
            GameObject marker = (GameObject) Instantiate(originMarker, origin.cachedTransform.position + Vector3.up * heightOverWaypoint, Quaternion.identity);
            marker.transform.parent = transform;
        }

        foreach (Waypoint goal in goals)
        {
            GameObject marker = (GameObject)Instantiate(goalMarker, goal.cachedTransform.position + Vector3.up * heightOverWaypoint, Quaternion.identity);
            marker.transform.parent = transform;
        }
    }


    void Update()
    {
        if (singleSpawn && _timePassedBy > _spawnInterval)
            return;

        _timePassedBy += Time.deltaTime;

        if (_timePassedBy > _spawnInterval)
        {
            _timePassedBy = 0;
            UpdateSpawnInterval();
            GenerateVehicle();
        }
    }

    void UpdateSpawnInterval()
    {
        _spawnInterval = spawnInterval * 0.7f + (10.0f/GUIGame.carsSize) * Random.Range(0f, 0.7f);
    }


    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        foreach (Waypoint goal in goals)
        {
            Gizmos.DrawSphere(goal.transform.position + Vector3.up * heightOverWaypoint, 1);
        }

        Gizmos.color = Color.green;
        foreach (Waypoint origin in origins)
        {
            Gizmos.DrawSphere(origin.transform.position + Vector3.up * heightOverWaypoint, 1);
        }
    }

    private void GenerateVehicle()
    {
        Waypoint start = origins[Random.Range(0, origins.Length)];
        Waypoint end = goals[Random.Range(0, goals.Length)];


        if (CollisionCheck(start.cachedTransform.position))
            return;


        GameObject vehiclePrefab = RandomizeVehicle();
        if (vehiclePrefab == null)
            return;

        GameObject vehicle = (GameObject)Instantiate(vehiclePrefab, start.cachedTransform.position, Quaternion.identity);
        vehicle.transform.parent = _cachedVehicleParent;

        VehicleMemory vehicleMemory = vehicle.GetComponentInChildren<VehicleMemory>();

        vehiclesContainer.AddVehicle(ref vehicleMemory);
        vehicleMemory.vehiclesContainer = vehiclesContainer;
        vehicleMemory.pathsContainer = pathsContainer;
        
        vehicleMemory.start = start;
        vehicleMemory.goal = end;
        vehicleMemory.recalculatePath = true; 
    }

    private GameObject RandomizeVehicle()
    {
        float value = Random.Range(0,100);
        float sum = 0f;
        float[] carPercentage = GUIGame.carPercentage;

        if (carPercentage != null)
        {
            for (int i = 0; i < carPercentage.Length; i++)
            {
                sum += carPercentage[i];

                if (sum >= value)
                {
                    return vehiclePrefabs[i];
                }

            }
        }
        

        return null;
    }

    private bool CollisionCheck(Vector3 position)
    {
        Collider[] colliderHitArray = Physics.OverlapSphere(position, collisionCheckRadius, _sightLayerMask);
        return colliderHitArray.Length > 0;
    }
}
