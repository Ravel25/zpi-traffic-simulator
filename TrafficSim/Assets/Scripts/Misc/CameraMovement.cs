﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour
{
    public Camera mainCamera;
    public float movementSpeed = 1f;
    public float rotationSpeed = 1f;
    public float zoomSpeed = 1f;
    public float movementMargin = 10f;
    public float shiftKeyMovementSpeedBonus = 45f;
    public float cursorToScreenBorderMinDistance = 30;
    
    private float _eastLimit, _westLimit, _southLimit, _northLimit;



    void Start()
    {
        _eastLimit = Terrain.activeTerrain.transform.position.x - movementMargin;
        _westLimit = Terrain.activeTerrain.transform.position.x + Terrain.activeTerrain.terrainData.size.x + movementMargin;
        _southLimit = Terrain.activeTerrain.transform.position.z - movementMargin;
        _northLimit = Terrain.activeTerrain.transform.position.z + Terrain.activeTerrain.terrainData.size.z + movementMargin;
    }

    void Update()
    {
        // MOVEMENT
        if (IsCameraMovementActive())
        {
            Vector3 movementVector = GetMovementVector();
            if (IsDesiredPositionInBoundaries(movementVector))
            {
                this.transform.Translate(movementVector);
            }
        }


        // DRAG
        if (Input.GetMouseButton(0))
        {

        }

        
        // ZOOMING
        float scrollMovement = Input.GetAxis("Mouse ScrollWheel");
        if (scrollMovement != 0)
        {
            Vector3 zoomVector = mainCamera.transform.forward * zoomSpeed * scrollMovement;
            this.transform.Translate(zoomVector, Space.World);
        }


        // ROTATION
        if (Input.GetMouseButton(2))
        {
            this.transform.Rotate(0, Input.GetAxis("Mouse X") * Time.deltaTime * rotationSpeed, 0);
            mainCamera.transform.Rotate(-Input.GetAxis("Mouse Y") * Time.deltaTime * rotationSpeed, 0, 0);
        }

    }


    

    private bool IsKeyboardMovementActive()
    {
        return (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) );
    }

    private bool IsMouseMovementActive()
    {
        float x = Input.mousePosition.x,
              y = Input.mousePosition.y; 

        return
        (
            (x < cursorToScreenBorderMinDistance && x > 0) ||
            (x > Screen.width - cursorToScreenBorderMinDistance && x < Screen.width) ||
            (y < cursorToScreenBorderMinDistance && y > 0) ||
            (y > Screen.height - cursorToScreenBorderMinDistance && y < Screen.height)
        );
    }

    private bool IsCameraMovementActive()
    {
        return IsKeyboardMovementActive() || IsMouseMovementActive();
    }

    private Vector3 GetMovementVector()
    {
        float xAxisMovement = 0f;
        float zAxisMovement = 0f;
        float speed = Input.GetKey(KeyCode.LeftShift) ? (movementSpeed + shiftKeyMovementSpeedBonus) : movementSpeed;
        speed *= Time.deltaTime;

        xAxisMovement -= Input.GetKey(KeyCode.A) ? speed : 0;
        xAxisMovement += Input.GetKey(KeyCode.D) ? speed : 0;

        zAxisMovement += Input.GetKey(KeyCode.W) ? speed : 0;
        zAxisMovement -= Input.GetKey(KeyCode.S) ? speed : 0;

        xAxisMovement -= Input.mousePosition.x < cursorToScreenBorderMinDistance ? speed : 0;
        xAxisMovement += Input.mousePosition.x > Screen.width - cursorToScreenBorderMinDistance ? speed : 0;

        zAxisMovement += Input.mousePosition.y > Screen.height - cursorToScreenBorderMinDistance ? speed : 0;
        zAxisMovement -= Input.mousePosition.y < cursorToScreenBorderMinDistance ? speed : 0;

        return new Vector3(xAxisMovement, 0, zAxisMovement);
    }


    

    public bool IsDesiredPositionInBoundaries(Vector3 desiredPosition)
    {
        if ((this.transform.position.x + desiredPosition.x) < _eastLimit)
            return false;

        if ((this.transform.position.x + desiredPosition.x) > _westLimit)
            return false;

        if ((this.transform.position.z + desiredPosition.z) > _northLimit)
            return false;

        if ((this.transform.position.z + desiredPosition.z) < _southLimit)
            return false;

        return true;
    }

}