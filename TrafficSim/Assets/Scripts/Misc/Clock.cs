﻿using UnityEngine;
using System.Collections;

public class Clock : MonoBehaviour 
{
    public int days, hours, minutes;
    public float factor = 1f;

    private float _elapsedTime;

    //void Start () 
    //{
	
    //}
	
	void Update () 
    {
        _elapsedTime += Time.deltaTime;

        if (_elapsedTime > factor)
        {
            _elapsedTime = 0;
            UpdateMinutes();
            UpdateHours();
        }
	}

    void UpdateMinutes()
    {
        minutes += 1;
        if( minutes == 60)
        {
            minutes = 0;
            hours += 1;
        }       
    }

    void UpdateHours()
    {
        if( hours == 24)
        {
            hours = 0;
            days += 1;
        }
            
    }

   
}
