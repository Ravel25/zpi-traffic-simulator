﻿using UnityEngine;
using System.Collections;

public class UnitSelectionAndFollow : MonoBehaviour
{
    public Camera mainCamera;
    public GameObject selectionHighlightPrefab;
    public GUIStyle speedSliderStyle;
    public float vehicleSpeedRefreshTime = 0.2f;

    GameObject _selectionHighlight;
    Transform _selectedTransform, _highlightTransform;
    CameraControllerScript _cameraController;
    int _vehiclesLayerMask, _selectedInstanceId, _signsLayerMask;
    VehicleMemory _selectedVehicle;
    bool _vehicleSelected, _signSelected;
    SpeedLimitSign _selectedSign;
    float _vehicleSpeed, _vehicleSpeedLastUpdateTime;


    void Start()
    {
        _cameraController = GetComponent<CameraControllerScript>();
        _selectionHighlight = (GameObject)Instantiate(selectionHighlightPrefab);
        _selectionHighlight.SetActive(false);
        _highlightTransform = _selectionHighlight.transform;
        _highlightTransform.parent = transform;
        _vehiclesLayerMask = LayerMask.GetMask("Vehicles");
        _signsLayerMask = LayerMask.GetMask("Speed limit signs");
    }

    void Update()
    {
        _vehicleSpeedLastUpdateTime += Time.deltaTime;

        if (Input.GetMouseButtonUp(0))
        {
            Ray vRay = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(vRay, out hitInfo, 100f, _vehiclesLayerMask))
            {
                VehicleMemory otherVehicle = hitInfo.collider.GetComponentInParent<VehicleMemory>();
                _selectedVehicle = otherVehicle;
                _cameraController.followTransform = otherVehicle.cachedComponents.transform;
                _cameraController.focusPivot = true;
                _selectedTransform = otherVehicle.cachedComponents.transform;
                _vehicleSelected = true;
                _signSelected = false;
            }
            else if (Physics.Raycast(vRay, out hitInfo, 100f, _signsLayerMask))
            {
                SpeedLimitSign sign = hitInfo.collider.GetComponentInParent<SpeedLimitSign>();
                _selectedSign = sign;
                _selectedTransform = sign.transform;
                //_cameraController.followTransform = _selectedTransform;
                //_cameraController.focusPivot = true;
                _signSelected = true;
                _vehicleSelected = false;
            }
            else
            {
                _vehicleSelected = false;
                //_signSelected = false;
                _cameraController.focusPivot = false;
            }

        }
        if (_selectedTransform == null)
            _vehicleSelected = false;

        if (!_cameraController.focusPivot)
            _vehicleSelected = false;

        HighlightSelectedUnit();
    }

    void HighlightSelectedUnit()
    {
        if (_vehicleSelected)
        {
            _highlightTransform.position = _selectedTransform.position;
            _highlightTransform.rotation = _selectedTransform.rotation;
            _selectionHighlight.SetActive(true);
        }
        else if (_signSelected)
        {
            _highlightTransform.position = _selectedTransform.position + Vector3.up * 0.2f;
            _highlightTransform.rotation = _selectedTransform.rotation;
            _selectionHighlight.SetActive(true);
        }
        else
            _selectionHighlight.SetActive(false);
    }

    void OnGUI()
    {

        if (_vehicleSelected && vehicleSpeedRefreshTime > _vehicleSpeedLastUpdateTime)
        {
            _vehicleSpeed = _selectedVehicle.speed;
            _vehicleSpeedLastUpdateTime = 0;
        }



        if (_vehicleSelected)
        {
            float left = Screen.width - 220,
                controllWidth = 190,
                margin = 5;

            GUI.Box(new Rect(left, 20, controllWidth + margin * 2, 180), "Informacje o samochodzie");
            GUI.Label(new Rect(left + margin, 50, controllWidth, 30), "Szybkość: " + _selectedVehicle.speed.ToString("F2"));

            GUI.Box(new Rect(left + margin, 80, controllWidth, 30), "");
            GUI.Box(new Rect(left + 2 * margin, 82, _selectedVehicle.speed / _selectedVehicle.maxSpeed * (controllWidth - 2 * margin), 26), "", speedSliderStyle);


            GUI.Label(new Rect(left + margin, 135, controllWidth, 30), "Maksymalna szybkość: " + _selectedVehicle.maxSpeed.ToString("F2"));
            GUI.Label(new Rect(left + margin, 170, controllWidth, 30), _selectedVehicle.gameObject.name.Split('(')[0]);

        }
        else if (_signSelected)
        {



            float left = Screen.width - 220,
                controllWidth = 190,
                margin = 5;

            GUI.Box(new Rect(left, 20, controllWidth + margin * 2, 100), "Limit prędkości");


            _selectedSign.speedLimit = GUI.HorizontalSlider(new Rect(left + margin, 74, controllWidth - 35, 50), _selectedSign.speedLimit, 0, _selectedSign.maxSpeedLimit);
            GUI.Label(new Rect(left + controllWidth - 25, 70, 40, 30), _selectedSign.speedLimit.ToString("F2"));

            if (GUI.Button(new Rect(left + controllWidth - 22, 20, 32, 20), "x"))
            {
                _signSelected = false;
            }
        }
    }
}
