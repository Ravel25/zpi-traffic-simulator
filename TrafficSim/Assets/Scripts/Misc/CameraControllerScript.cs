﻿using UnityEngine;
using System.Collections;

public class CameraControllerScript : MonoBehaviour
{

    public Camera mainCamera;
    public Transform tiltPivot, followTransform;
    public float movementSpeed = 1f,
                 rotationSpeed = 1f,
                 zoomSpeed = 1f,
                 movementMargin = 10f,
                 shiftKeyMovementSpeedBonus = 45f,
                 cursorToScreenBorderMinDistance = 30,
                 minCameraDistance = 1f,
                 panSpeed = 10f,
                 maxDeltaTime = 0.15f;

    public bool debugModeEnabled, focusPivot;

    private float _eastLimit, _westLimit, _southLimit, _northLimit, _cameraDistance, _deltaTime;
    private Transform _cachedTransform, _cameraCachedTransform, _tiltPivotCachedTransform;


    void Start()
    {
        _eastLimit = Terrain.activeTerrain.transform.position.x - movementMargin;
        _westLimit = Terrain.activeTerrain.transform.position.x + Terrain.activeTerrain.terrainData.size.x + movementMargin;
        _southLimit = Terrain.activeTerrain.transform.position.z - movementMargin;
        _northLimit = Terrain.activeTerrain.transform.position.z + Terrain.activeTerrain.terrainData.size.z + movementMargin;
        _cachedTransform = this.transform;
        _cameraCachedTransform = mainCamera.transform;
        _tiltPivotCachedTransform = tiltPivot.transform;
    }

    void Update()
    {
        _deltaTime = Time.deltaTime > maxDeltaTime ? maxDeltaTime : Time.deltaTime;
        _cameraDistance = Vector3.Distance(_cachedTransform.position, _cameraCachedTransform.position);
        if (followTransform == null)
            focusPivot = false;

        // MOVEMENT
        if (IsCameraMovementActive())
        {
            focusPivot = false;
            Vector3 movementVector = GetMovementVector();
            _cachedTransform.Translate(movementVector);
        }


        // DRAG
        if (Input.GetMouseButton(2))
        {
            focusPivot = false;
            _cachedTransform.Translate(new Vector3(-Input.GetAxis("Mouse X") * _deltaTime * panSpeed * _cameraDistance, 0, -Input.GetAxis("Mouse Y") * _deltaTime * panSpeed * _cameraDistance));
        }


        // ZOOMING
        float scrollMovement = Input.GetAxis("Mouse ScrollWheel");
        if (scrollMovement > 0 && _cameraDistance > minCameraDistance || scrollMovement < 0)
        {
            float zoomSpeed = Input.GetKey(KeyCode.LeftShift) ? (this.zoomSpeed + shiftKeyMovementSpeedBonus) : this.zoomSpeed;

            Vector3 zoomVector = _cameraCachedTransform.forward * zoomSpeed * scrollMovement;
            _cameraCachedTransform.Translate(zoomVector, Space.World);
        }


        // ROTATION
        if (Input.GetMouseButton(1))
        {
            _cachedTransform.Rotate(0, Input.GetAxis("Mouse X") * _deltaTime * rotationSpeed, 0);

            //if (Input.GetAxis("Mouse Y") > 0 && _tiltPivotCachedTransform.rotation.eulerAngles.x < 10)
            //    return;
            //if (Input.GetAxis("Mouse Y") < 0 && _tiltPivotCachedTransform.rotation.eulerAngles.x > 80)
            //    return;
   
            _tiltPivotCachedTransform.Rotate(-Input.GetAxis("Mouse Y") * _deltaTime * rotationSpeed, 0, 0);

            Vector3 rotation = _tiltPivotCachedTransform.rotation.eulerAngles;
            if (rotation.x < 10)
                rotation.x = 10;
            if (rotation.x > 80)
                rotation.x = 80;

            _tiltPivotCachedTransform.rotation = Quaternion.Euler(rotation);
        }

        if (focusPivot)
        {
           _cachedTransform.position = followTransform.position;
        }

    }


    void OnDrawGizmos()
    {
        if (debugModeEnabled)
        {
            if (_cachedTransform == null)
                _cachedTransform = this.transform;

            Gizmos.DrawSphere(_cachedTransform.position, 1);
        }
    }

    private bool IsKeyboardMovementActive()
    {
        return (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D));
    }

    private bool IsMouseMovementActive()
    {
        float x = Input.mousePosition.x,
              y = Input.mousePosition.y;

        return
        (
            (x < cursorToScreenBorderMinDistance && x > 0) ||
            (x > Screen.width - cursorToScreenBorderMinDistance && x < Screen.width) ||
            (y < cursorToScreenBorderMinDistance && y > 0) ||
            (y > Screen.height - cursorToScreenBorderMinDistance && y < Screen.height)
        );
    }

    private bool IsCameraMovementActive()
    {
        return IsKeyboardMovementActive();// || IsMouseMovementActive();
    }

    private Vector3 GetMovementVector()
    {
        float xAxisMovement = 0f;
        float zAxisMovement = 0f;
        float speed = Input.GetKey(KeyCode.LeftShift) ? (movementSpeed + shiftKeyMovementSpeedBonus) : movementSpeed;
        speed *= _deltaTime;

        xAxisMovement -= Input.GetKey(KeyCode.A) ? speed : 0;
        xAxisMovement += Input.GetKey(KeyCode.D) ? speed : 0;

        zAxisMovement += Input.GetKey(KeyCode.W) ? speed : 0;
        zAxisMovement -= Input.GetKey(KeyCode.S) ? speed : 0;

        xAxisMovement -= Input.mousePosition.x < cursorToScreenBorderMinDistance ? speed : 0;
        xAxisMovement += Input.mousePosition.x > Screen.width - cursorToScreenBorderMinDistance ? speed : 0;

        zAxisMovement += Input.mousePosition.y > Screen.height - cursorToScreenBorderMinDistance ? speed : 0;
        zAxisMovement -= Input.mousePosition.y < cursorToScreenBorderMinDistance ? speed : 0;

        return new Vector3(xAxisMovement, 0, zAxisMovement);
    }

    public bool IsDesiredPositionInBoundaries(Vector3 desiredPosition)
    {
        if ((_cachedTransform.position.x + desiredPosition.x) < _eastLimit)
            return false;

        if ((_cachedTransform.position.x + desiredPosition.x) > _westLimit)
            return false;

        if ((_cachedTransform.position.z + desiredPosition.z) > _northLimit)
            return false;

        if ((_cachedTransform.position.z + desiredPosition.z) < _southLimit)
            return false;

        return true;
    }

    public void move(int key)
    {
        float xAxisMovement = 0f;
        float zAxisMovement = 0f;
        float speed = Input.GetKey(KeyCode.LeftShift) ? (movementSpeed + shiftKeyMovementSpeedBonus) : movementSpeed;
        speed *= 0.6f;

        switch (key)
        {
            case 0:
                xAxisMovement -= speed;
                break;
            case 1:
                xAxisMovement += speed;
                break;
            case 2:
                zAxisMovement += speed;
                break;
            case 3:
                zAxisMovement -= speed;
                break;

            default:
                break;
        }

        xAxisMovement -= Input.mousePosition.x < cursorToScreenBorderMinDistance ? speed : 0;
        xAxisMovement += Input.mousePosition.x > Screen.width - cursorToScreenBorderMinDistance ? speed : 0;

        zAxisMovement += Input.mousePosition.y > Screen.height - cursorToScreenBorderMinDistance ? speed : 0;
        zAxisMovement -= Input.mousePosition.y < cursorToScreenBorderMinDistance ? speed : 0;

        _cachedTransform.Translate(new Vector3(xAxisMovement, 0, zAxisMovement));
    }
}
