﻿using UnityEngine;
using System.Collections;

public class SpeedLimitEndSign : MonoBehaviour 
{
	
	void OnTriggerEnter(Collider collider)
	{
		VehicleMemory vehicle = collider.GetComponent<VehicleMemory>();
		if (vehicle != null)
		{
			vehicle.speedLimitEnabled = false;
		}
	}
	
	
}