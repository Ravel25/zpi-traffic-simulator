﻿using UnityEngine;
using System.Collections;

public class SpeedLimitSign : MonoBehaviour 
{
    public float speedLimit;
    public float maxSpeedLimit = 10f;

    void OnTriggerEnter(Collider collider)
    {
        VehicleMemory vehicle = collider.GetComponent<VehicleMemory>();
        if (vehicle != null)
        {
            vehicle.speedLimitEnabled = true;
            vehicle.speedLimit = speedLimit;
        }
    }



    
}
