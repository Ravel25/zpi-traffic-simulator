﻿using UnityEngine;
using System.Collections.Generic;

public class AStarPathfinding
{
    public static int vehiclesCalculatingPath;


    public Waypoint[] FindPath(Waypoint start, Waypoint goal)
    {
        
        vehiclesCalculatingPath++;
        //Debug.Log(vehiclesCalculatingPath);

        IDictionary<Waypoint, int> frontier = new Dictionary<Waypoint, int>(); // pair: <waypoint , priority>
        IDictionary<Waypoint, int> accumulatedCost = new Dictionary<Waypoint, int>(); // closed list with acc cost
        IDictionary<Waypoint, Waypoint> parents = new Dictionary<Waypoint, Waypoint>(); // used to reacreate path

        Waypoint current = start;
        accumulatedCost.Add(current, 0);
        frontier.Add(start, ManhattanDistance(start, goal));
        float? bestScore = null;

        while (frontier.Count > 0)
        {
            Waypoint bestNode = null;
           
            float currentCost = frontier[current];

            if (current.availableWaypoints != null)
            {
                for(int i = 0; i < current.availableWaypoints.Length; i++)
                {
                    Waypoint next = current.availableWaypoints[i];
                    int nextDistance = current.availableWaypointsWeights[i];
                    int nextAcc = accumulatedCost[current] + nextDistance;
                    int admissibleHeuristics = ManhattanDistance(next, goal);
                    int priority = nextAcc + admissibleHeuristics;

                    if (!bestScore.HasValue || priority <= bestScore)
                    {
                        if (next == goal)
                        {
                            bestScore = priority;
                            if (parents.ContainsKey(next))
                            {
                                if (nextAcc < accumulatedCost[next])
                                {
                                    accumulatedCost[next] = nextAcc;
                                    parents[next] = current;
                                }
                            }
                            else
                            {
                                    accumulatedCost.Add(next,nextAcc);
                                    parents.Add(next, current);
                                                                
                            }
                            continue;
                        }

                        if (!accumulatedCost.ContainsKey(next))
                        {
                            accumulatedCost.Add(next, nextAcc);
                            frontier.Add(next, priority);
                            parents.Add(next, current);
                        }
                        else if (nextAcc < accumulatedCost[next])
                        {
                            accumulatedCost[next] = nextAcc;
                            frontier[next] = priority;
                            //frontier.Add(next, priority);
                            parents[next] = current;
                        }

                        if (priority <= currentCost)
                        {
                            bestNode = next;

                            if (!frontier.ContainsKey(bestNode))
                            {
                                frontier.Add(bestNode, priority);
                            }
                        }
                    }
                }
            }
           
            // do usuniecia
            if (!frontier.ContainsKey(current))
            {
                Debug.Log("A* Crash");
            }
            frontier.Remove(current);
            current = bestNode != null ? bestNode : GetBestNode(frontier);
        }
        vehiclesCalculatingPath--;
        return ReconstructPath(parents, goal, start);
    }


    private Waypoint GetBestNode(IDictionary<Waypoint, int> dict)
    {
        Waypoint bestNode = null;
        int bestScore = int.MaxValue;
        foreach (KeyValuePair<Waypoint, int> p in dict)
        {
            if (p.Value < bestScore)
                bestNode = p.Key;
        }
        return bestNode;
    }


    private Waypoint[] ReconstructPath(IDictionary<Waypoint, Waypoint> cameFrom, Waypoint current, Waypoint start)
    {
        List<Waypoint> path = new List<Waypoint>();
        path.Add(current);

        while (current != start)
        {
            if (!cameFrom.ContainsKey(current))
                return null;

            current = cameFrom[current];
            path.Add(current);
        }
        path.Reverse();

        return path.ToArray();
    }

    private int ManhattanDistance(Waypoint node, Waypoint goal)
    {
        return Mathf.Abs(goal.gridX - node.gridX) + Mathf.Abs(goal.gridY - node.gridY);
    }
}