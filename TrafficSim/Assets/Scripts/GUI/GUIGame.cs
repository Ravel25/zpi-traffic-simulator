﻿using UnityEngine;
using System.Collections;

public class GUIGame : MonoBehaviour
{
    public static float[] carPercentage;

    public static float carsSize = 5.0f;
    public string mainMenuScene;

    bool isCollapsed = false;

    void Start() 
    {
        carPercentage = new float[5];

        for (int i = 0; i < carPercentage.Length; i++)
        {
            carPercentage[i] = 20.0f;
        }
    }

    

    void Update()
    {
        if (Input.GetKey("escape"))
        {
            Application.LoadLevel(mainMenuScene);
        }
    }

    


    void OnGUI()
    {
        float change = 0;
        bool done = false;
        float odd = 0;


        if (!isCollapsed)
        {
            GUI.Box(new Rect(0, 0, 275, 460), "Parametry symulacji");
        
            GUI.Label(new Rect(25, 35, 300, 30), "Bardzo agresywni kierowcy");// "Przesuń w prawo aby zwiększyć ilość bardzo agresywnych kierowców"));
            change = GUI.HorizontalSlider(new Rect(50, 60, 170, 30), carPercentage[0], 0.0f, 100.0f);
            GUI.Label(new Rect(230, 55, 45, 30), (int)carPercentage[0] + " %");

            GUI.Box(new Rect(25, 35, 170, 40), new GUIContent("", "Przesuń w prawo aby zwiększyć ilość bardzo agresywnych kierowców"), GUIStyle.none);
            GUI.Label(new Rect(50, 370, 170, 60), GUI.tooltip);
            GUI.tooltip = null;


            if (!done && isChange(change, carPercentage[0]))
            {
                odd = (change - carPercentage[0]) / 4.0f;

                changeValues(0, odd);

                carPercentage[0] = change;
                done = true;
            }

            GUI.Label(new Rect(25, 90, 300, 30), "Agresywni kierowcy");
            change = GUI.HorizontalSlider(new Rect(50, 115, 170, 30), carPercentage[1], 0.0f, 100.0f);
            GUI.Label(new Rect(230, 110, 45, 30), (int)carPercentage[1] + " %");

            GUI.Box(new Rect(25, 90, 170, 40), new GUIContent("", "Przesuń w prawo aby zwiększyć ilość agresywnych kierowców"), GUIStyle.none);
            GUI.Label(new Rect(50, 370, 170, 60), GUI.tooltip);
            GUI.tooltip = null;


            if (!done && isChange(change, carPercentage[1]))
            {
                odd = (change - carPercentage[1]) / 4.0f;

                changeValues(1, odd);

                carPercentage[1] = change;
                done = true;
            }

            GUI.Label(new Rect(25, 145, 300, 30), "Przeciętni kierowcy");
            change = GUI.HorizontalSlider(new Rect(50, 170, 170, 30), carPercentage[2], 0.0f, 100.0f);
            GUI.Label(new Rect(230, 165, 45, 30), (int)carPercentage[2] + " %");

            GUI.Box(new Rect(50, 145, 170, 40), new GUIContent("", "Przesuń w prawo aby zwiększyć ilość przeciętnych kierowców"), GUIStyle.none);
            GUI.Label(new Rect(50, 370, 170, 60), GUI.tooltip);
            GUI.tooltip = null;


            if (!done && isChange(change, carPercentage[2]))
            {
                odd = (change - carPercentage[2]) / 4.0f;

                changeValues(2, odd);

                carPercentage[2] = change;
                done = true;
            }

            GUI.Label(new Rect(25, 200, 300, 30), "Ostrożni kierowcy");
            change = GUI.HorizontalSlider(new Rect(50, 225, 170, 30), carPercentage[3], 0.0f, 100.0f);
            GUI.Label(new Rect(230, 220, 45, 30), (int)carPercentage[3] + " %");

            GUI.Box(new Rect(50, 200, 170, 40), new GUIContent("", "Przesuń w prawo aby zwiększyć ilość ostrożnych kierowców"), GUIStyle.none);
            GUI.Label(new Rect(50, 370, 170, 60), GUI.tooltip);
            GUI.tooltip = null;

            if (!done && isChange(change, carPercentage[3]))
            {
                odd = (change - carPercentage[3]) / 4.0f;

                changeValues(3, odd);

                carPercentage[3] = change;
                done = true;
            }
            GUI.Label(new Rect(25, 255, 300, 30), "Bardzo ostrożni kierowcy");
            change = GUI.HorizontalSlider(new Rect(50, 280, 170, 30), carPercentage[4], 0.0f, 100.0f);
            GUI.Label(new Rect(230, 275, 45, 30), (int)carPercentage[4] + " %");

            GUI.Box(new Rect(50, 255, 170, 40), new GUIContent("", "Przesuń w prawo aby zwiększyć ilość bardzo ostrożnych kierowców"), GUIStyle.none);
            GUI.Label(new Rect(50, 370, 170, 60), GUI.tooltip);
            GUI.tooltip = null;



            if (!done && isChange(change, carPercentage[4]))
            {

                odd = (change - carPercentage[4]) / 4.0f;

                changeValues(4, odd);

                carPercentage[4] = change;
                done = true;
            }


            GUI.Label(new Rect(25, 320, 300, 30), "Ilość pojazdów: " + VehiclesContainer.vehicles.Count.ToString());//, "Przesuń w prawo aby zwiększyć ogólną ilość kierowców na drogach"));
            carsSize = GUI.HorizontalSlider(new Rect(50, 345, 170, 30), carsSize, 1.0f, 100.0f);

            GUI.Box(new Rect(50, 320, 170, 40), new GUIContent("", "Przesuń w prawo aby zwiększyć ogólną ilość kierowców na drogach"), GUIStyle.none);
            GUI.Label(new Rect(50, 370, 170, 60), GUI.tooltip);
            GUI.tooltip = null;

            if (GUI.Button(new Rect(210, 3, 60, 20), "zwiń"))
            {
                isCollapsed = !isCollapsed;
            }

        }
        else
        {
            GUI.Box(new Rect(0, 0, 275, 30), "Parametry symulacji");
            //GUI.Label(new Rect(0, 55, 45, 30), (int)carPercentage[0] + " %");
            //GUI.Label(new Rect(0, 110, 45, 30), (int)carPercentage[1] + " %");
            //GUI.Label(new Rect(0, 165, 45, 30), (int)carPercentage[2] + " %");
            //GUI.Label(new Rect(0, 220, 45, 30), (int)carPercentage[3] + " %");
            //GUI.Label(new Rect(0, 275, 45, 30), (int)carPercentage[4] + " %");

            if (GUI.Button(new Rect(210, 3, 60, 20), "rozwiń"))
            {
                isCollapsed = !isCollapsed;
            }
        }

        //GUI.Box(new Rect(50, 515, 275, 40), new GUIContent("", "Przesuń w prawo aby zwiększyć ogólną ilość kierowców na drogach"), GUIStyle.none);
        //GUI.Label(new Rect(50, 515, 275, 40), GUI.tooltip);
        //GUI.tooltip = null;

        //GUI.skin.button.normal.background = top;
        ////top
        //if (GUI.Button(new Rect(((int)(Screen.width / 2.0)) - 3, Screen.height - 100, 20, 20), top))
        //{
        //    _cameraController.move(2);
        //}

        ////down
        //if (GUI.Button(new Rect(((int)(Screen.width / 2.0)) - 3, Screen.height - 50, 40, 40), down))
        //{
        //    _cameraController.move(3);
        //}

        ////left
        //if (GUI.Button(new Rect(((int)(Screen.width / 2.0)) - 25, Screen.height - 75, 20, 20), left))
        //{
        //    _cameraController.move(0);
        //}

        ////right
        //if (GUI.Button(new Rect(((int)(Screen.width / 2.0)) + 20, Screen.height - 75, 20, 20), right))
        //{
        //    _cameraController.move(1);
        //}
     }


    private bool isChange(float currentValue, float lastValue)
    {

        if (currentValue != lastValue)
        {
            return true;
        }

        return false;
    }

    private void changeValues(int current, float odd)
    {
        float sum = 0f;
        int zeroSize = 0;

        for (int i = 0; i < carPercentage.Length; i++)
        {
            if (i != current)
            {
                if ((carPercentage[i] - odd) < 0)
                {
                    //szczególny przypadek
                    sum += 0 - (carPercentage[i] - odd);
                    carPercentage[i] = 0;
                    zeroSize++;
                }
                else
                {
                    carPercentage[i] -= odd;
                }
            }
        }

        
        while (sum != 0)
        {
            odd = sum / (4 - zeroSize);
            sum = 0;

            for (int i = 0; i < carPercentage.Length; i++)
            {
                if (i != current && carPercentage[i] != 0f)
                {
                    if ((carPercentage[i] - odd) < 0)
                    {
                        //szczególny przypadek
                        sum += 0 - (carPercentage[i] - odd);
                        carPercentage[i] = 0;
                        zeroSize++;
                    }
                    else
                    {
                        carPercentage[i] -= odd;
                    }
                }
            }

            if (zeroSize == 3)
            {
                for (int i = 0; i < carPercentage.Length; i++)
                {
                    if (i != current && carPercentage[i] != 0f)
                    {
                        carPercentage[i] -= sum;
                        return;
                    }
                }
            }
        }
    }
    

     
}
