﻿using UnityEngine;
using System.Collections;

public class SceneSelection : MonoBehaviour
{
    public string[] scenes;
    public Texture background;


    void OnGUI() 
    {
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), background, ScaleMode.ScaleAndCrop);

        GUI.Box(new Rect(50, 50, 300, scenes.Length * 70 + 150), "Wybór mapy");

        int i;
        for(i = 0 ; i < scenes.Length; i++)
        {
            if (GUI.Button(new Rect(75, i * 70 + 100, 250, 50), scenes[i]))
            {
                Application.LoadLevel(scenes[i]);
            }
        }

        if (GUI.Button(new Rect(75, i * 70 + 125, 250, 50), "Quit"))
            Application.Quit();
    }

}