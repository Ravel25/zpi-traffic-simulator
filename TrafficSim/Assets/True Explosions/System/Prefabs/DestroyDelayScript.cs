﻿using UnityEngine;
using System.Collections;

public class DestroyDelayScript : MonoBehaviour 
{
    public float delay;

    float _timePassedBy;
    

	void Update () 
    {
        _timePassedBy += Time.deltaTime;
        if (_timePassedBy > delay)
        {
            Destroy(this.gameObject);
        }
	}
}
