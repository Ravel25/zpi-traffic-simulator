using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class SetVehicleSpeedAction : RAINAction
{
    private VehicleMemory _memory;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
        VehicleMemoryAIExtension memoryExtension = ai.GetCustomElement<VehicleMemoryAIExtension>();
        _memory = memoryExtension.memory;
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        // max available acceleration is current acceleration, lowest deceleration will be used to prevent from accident
        float acceleration = _memory.maxAcceleration;


        if (_memory.trafficLightsInFront)
        {
            // if vehicle cant stop before lights it doesnt even try
            float deceleration = GetDeceleration(0, _memory.frontTrafficLightsDistance, 2f);
            float distanceToStop = GetDistanceToStop(_memory.speed);
            if (deceleration <= _memory.maxDeceleration && _memory.frontTrafficLightsDistance + 2f > distanceToStop)
                acceleration = deceleration;
        }
        

        if (_memory.vehicleInFront)
        {
            float deceleration = GetDeceleration(_memory.frontVehicleSpeed, _memory.frontVehicleDistance, _memory.safeDistance);
            if (deceleration <= _memory.maxDeceleration && deceleration < acceleration)
                acceleration = deceleration;
        }


        // slowdown before next action
        if (_memory.arrivingDistance > 0 && (_memory.isOnCrossing || _memory.laneChangeAhead && !_memory.canChangeLane))
        {
            float deceleration = GetDeceleration(0, _memory.arrivingDistance, 0.2f);
            if (deceleration <= _memory.maxDeceleration && deceleration < acceleration)
                acceleration = deceleration;
        }


        // problem tutaj
        if (_memory.vehicleInFront && _memory.vehicleInFrontOnCrossing && _memory.vehicleInFrontTurning && _memory.vehicleInFrontTurningLeft)
        {
            if (_memory.crossingInFront)
            {
                float deceleration = GetDeceleration(0, _memory.crossingDistance, 0);
                if (deceleration <= _memory.maxDeceleration && deceleration < acceleration)
                    acceleration = deceleration;
            }
            else
            {
                acceleration = _memory.cachedComponents.vehicleSpecs.maxDeceleration;
            }
        }


        if (_memory.isTurnAhead &&
            _memory.speed > _memory.maxTurnSpeed &&
            acceleration > _memory.maxDeceleration)
        {
            acceleration = _memory.maxDeceleration;
        }

        if (_memory.speedLimitEnabled &&  _memory.speed > _memory.speedLimit &&  acceleration > _memory.maxDeceleration)
        {
            acceleration = _memory.maxDeceleration;
        }


        // seldom distance update may cause some weird speed changes
        if (_memory.acceleration < 0 && acceleration > 0)
            acceleration = 0;
        else if (_memory.acceleration > 0 && acceleration < 0)
            acceleration = 0;

        _memory.acceleration = acceleration;
        _memory.speed += acceleration * ai.DeltaTime;
        
        TrimSpeed();    
        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }

    private float GetDeceleration(float otherSpeed, float distance, float safeDistance)
    {
        if (distance > safeDistance)
        {
            return Mathf.Max((otherSpeed * otherSpeed - _memory.speed * _memory.speed) / (2 * (distance - safeDistance)), _memory.cachedComponents.vehicleSpecs.maxDeceleration);
        }
        else
        {
            return _memory.cachedComponents.vehicleSpecs.maxDeceleration;
        }
    }

    float GetDistanceToStop(float speed)
    {
        return -speed * speed / 2 / _memory.cachedComponents.vehicleSpecs.maxDeceleration;
    }

    private void TrimSpeed()
    {
        if (_memory.speed > _memory.maxSpeed)
            _memory.speed = _memory.maxSpeed;
        else if (_memory.speed < 0)
            _memory.speed = 0;
    }

}