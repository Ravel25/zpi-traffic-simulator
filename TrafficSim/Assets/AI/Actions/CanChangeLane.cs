﻿using UnityEngine;
using System.Collections;
using RAIN.Action;
using RAIN.Core;

[RAINDecision]
public class CanChangeLane : RAINDecision
{

    private VehicleMemory _memory;
    private int _lastRunning = 0;
        
    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
        VehicleMemoryAIExtension memoryExtension = ai.GetCustomElement<VehicleMemoryAIExtension>();
        _memory = memoryExtension.memory;
        _memory.arrivingDistance = (_memory.path[_memory.changePoint].cachedTransform.position - _memory.cachedComponents.transform.position).magnitude;
        _lastRunning = 0; 
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        if (_memory.arrivingDistance < 0.5f && !CanLeave())
        {
            return ActionResult.FAILURE;
        }
        //wykonanie wszystkich akcji
        ActionResult tResult = ActionResult.SUCCESS;

        for (; _lastRunning < _children.Count; _lastRunning++)
        {
            tResult = _children[_lastRunning].Run(ai);
            if (tResult != ActionResult.SUCCESS)
                break;
        }


        return tResult;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }

    bool CanLeave()
    {
        foreach (VehicleMemory vehicle in _memory.visibleVehicles)
        {
            float angle = Vector3.Angle(_memory.cachedComponents.transform.forward, vehicle.cachedComponents.transform.forward);
            if (angle  < 10)
            {
                float timeToCrossing;
                float distance = (vehicle.cachedComponents.transform.position - _memory.cachedComponents.transform.position).magnitude;
               
                if (vehicle.speed != 0)
                {
                    timeToCrossing = distance / vehicle.speed;
                }
                else
                {
                    timeToCrossing = float.MaxValue;
                }
                if (_memory.timeToLeaveCrossing > timeToCrossing)
                    return false;
            }
        }
        foreach (VehicleMemory vehicle in _memory.smelledVehicles)
        {
            float angleBetweenDirections = Vector3.Angle(_memory.cachedComponents.transform.forward, vehicle.cachedComponents.transform.forward);

            if (angleBetweenDirections < 10)
            {
                Vector3 vectorToVehicle = vehicle.cachedComponents.transform.position - _memory.cachedComponents.transform.position;
                float angleBetweenVehicles = Vector3.Angle(_memory.cachedComponents.transform.forward, vectorToVehicle);
                if (angleBetweenVehicles < 100)
                {
                    float timeToCrossing;
                    float distance = (vehicle.cachedComponents.transform.position - _memory.cachedComponents.transform.position).magnitude;
                    
                    if (vehicle.speed != 0)
                    {
                        timeToCrossing = distance / vehicle.speed;
                    }
                    else
                    {
                        timeToCrossing = float.MaxValue;
                    }

                    if (_memory.timeToLeaveCrossing > timeToCrossing)
                        return false;
                }
            }
        }

        return true;
    }
}
