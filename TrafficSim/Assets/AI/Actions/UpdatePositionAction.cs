using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class UpdatePositionAction : RAINAction
{
    private VehicleMemory _memory;
    private Transform _cachedTransform;


    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
        VehicleMemoryAIExtension memoryExtension = ai.GetCustomElement<VehicleMemoryAIExtension>();
        _memory = memoryExtension.memory;
        _cachedTransform = _memory.cachedComponents.transform;
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        ai.Kinematic.Orientation = _memory.orientation;
        ai.Kinematic.Velocity = _memory.speed * _cachedTransform.forward;
        //ai.Kinematic.Acceleration = _memory.acceleration * _cachedTransform.forward;
        
        return ActionResult.SUCCESS; 
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}