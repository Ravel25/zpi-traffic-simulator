using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class TurnAction : RAINAction
{
    private VehicleMemory _memory;
    private Transform _cachedTransform;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
        VehicleMemoryAIExtension memoryExtension = ai.GetCustomElement<VehicleMemoryAIExtension>();
        _memory = memoryExtension.memory;
        _cachedTransform = _memory.cachedComponents.transform;
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        
        Vector3 vectorToWaypoint = _memory.GetCurrentWaypoint().cachedTransform.position - _cachedTransform.position;
        _memory.orientation = Quaternion.LookRotation(Vector3.RotateTowards(_cachedTransform.forward, vectorToWaypoint, _memory.maxRotationSpeed * ai.DeltaTime, 0.0f)).eulerAngles;
        //_memory.orientation = Quaternion.LookRotation(Vector3.RotateTowards(_cachedTransform.forward, vectorToWaypoint, _vehicleSpecs.rotationSpeed * _drivingStyle.rotationFactor * (ai.Time - _memory.priorUpdateTime), 0.0f)).eulerAngles;

        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}