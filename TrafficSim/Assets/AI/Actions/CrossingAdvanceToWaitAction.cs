using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINDecision]
public class CrossingAdvanceToWaitDecision : RAINDecision
{
    private VehicleMemory _memory;
    private int _lastRunning = 0;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
        VehicleMemoryAIExtension memoryExtension = ai.GetCustomElement<VehicleMemoryAIExtension>();
        _memory = memoryExtension.memory;
        _lastRunning = 0;
        _memory.arrivingDistance = (_memory.path[_memory.crossingInitialWaypointIndex + _memory.crossingWaypointsAdvance].cachedTransform.position - _memory.cachedComponents.transform.position).magnitude;
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        if ((_memory._currentWaypointIndex - _memory.crossingInitialWaypointIndex) >= _memory.crossingWaypointsAdvance && (_memory.speed == 0 && _memory.arrivingDistance < 0.5f))
        {
            _memory.arrivingDistance = 0;
            return ActionResult.FAILURE;
        }

        ActionResult tResult = ActionResult.SUCCESS;
        
        for (; _lastRunning < _children.Count; _lastRunning++)
        {
            tResult = _children[_lastRunning].Run(ai);
            if (tResult != ActionResult.SUCCESS)
                break;
        }


        return tResult;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}