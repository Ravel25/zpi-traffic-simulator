using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class SwitchingLaneCheckOpportunityAction : RAINAction
{
    //private VehicleMemory _memory;
    private VehicleSight _sight;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
        VehicleMemoryAIExtension memoryExtension = ai.GetCustomElement<VehicleMemoryAIExtension>();
        //_memory = memoryExtension.memory;
        _sight = memoryExtension.sight;
        _sight.runBackSight = _sight.runLaneChangeAwareness = true;
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        return ActionResult.SUCCESS;
    }



    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}