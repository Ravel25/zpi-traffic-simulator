using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class AccelerateAction : RAINAction
{
    private VehicleMemory _memory;
    private VehicleSpecs _vehicleSpecs;
    private VehicleDrivingStyle _drivingStyle;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
        VehicleMemoryAIExtension memoryExtension = ai.GetCustomElement<VehicleMemoryAIExtension>();
        _memory = memoryExtension.memory;
        _vehicleSpecs = _memory.cachedComponents.vehicleSpecs;
        _drivingStyle = _memory.cachedComponents.drivingStyle;
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        float acceleration = _vehicleSpecs.maxAcceleration * _drivingStyle.accelerationFactor;
        if (_memory.speed < _vehicleSpecs.maxSpeed * _drivingStyle.speedFactor)
        {
            _memory.speed += acceleration * ai.DeltaTime;
        }
        

        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}