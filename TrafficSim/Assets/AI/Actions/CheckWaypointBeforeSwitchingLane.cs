using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class CheckWaypointBeforeSwitchingLane : RAINAction
{
    private VehicleMemory _memory;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
        VehicleMemoryAIExtension memoryExtension = ai.GetCustomElement<VehicleMemoryAIExtension>();
        _memory = memoryExtension.memory;
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        if (_memory.IsWaypointAcceptedOnCrossing(_memory.cachedComponents.transform.position) &&
            (_memory._currentWaypointIndex < _memory.laneChangeAwaitingWaypointIndex))
            _memory.MoveToNextWaypoint();

        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}