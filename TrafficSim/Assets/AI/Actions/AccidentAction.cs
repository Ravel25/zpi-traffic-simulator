using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class AccidentAction : RAINAction
{
    VehicleMemory _memory;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
        VehicleMemoryAIExtension memoryExtension = ai.GetCustomElement<VehicleMemoryAIExtension>();
        _memory = memoryExtension.memory;
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        _memory.timeSinceAccident += ai.DeltaTime;
        if (!_memory.exploded && _memory.timeSinceAccident > _memory.explosionDelay)
        {
            _memory.exploded = true;
            _memory.Explode();     
        }

        return ActionResult.RUNNING;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}