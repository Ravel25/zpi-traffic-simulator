using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class CalculatePathAction : RAINAction
{
    VehicleMemory _memory;

    public override void Start(AI ai)
    {
        base.Start(ai);
        //memory = ai.Body.GetComponent<VehicleMemory>();
        VehicleMemoryAIExtension memoryExtension = ai.GetCustomElement<VehicleMemoryAIExtension>();
        _memory = memoryExtension.memory;
    }

    public override ActionResult Execute(AI ai)
    {
        if(_memory.start == null || _memory.goal == null)
            return ActionResult.FAILURE;


        Waypoint[] cachedPath = _memory.pathsContainer.TryGetPath(_memory.start.GetInstanceID(), _memory.goal.GetInstanceID());
        if (cachedPath != null && cachedPath.Length > 0)
        {
            SetPath(cachedPath);
            return ActionResult.SUCCESS;
        }
        else if (cachedPath != null && cachedPath.Length == 0)
        {
            MonoBehaviour.Destroy(ai.Body);
            return ActionResult.FAILURE;
        }
        else
        {
            AStarPathfinding pathfinder = new AStarPathfinding();
            Waypoint[] path = pathfinder.FindPath(_memory.start, _memory.goal);

            if (path == null)
            {
                _memory.pathsContainer.AddPath(_memory.start.GetInstanceID(), _memory.goal.GetInstanceID(), new Waypoint[0]);
                MonoBehaviour.Destroy(ai.Body);
                return ActionResult.FAILURE;
            }

            else
            {
                _memory.pathsContainer.AddPath(_memory.start.GetInstanceID(), _memory.goal.GetInstanceID(), path);
                SetPath(path);
                return ActionResult.SUCCESS;
            }
        } 
    }


    void SetPath(Waypoint[] path)
    {
        _memory._currentWaypointIndex = 0;
        _memory.path = path;
        _memory.recalculatePath = false;

        if (path.Length > 1)
        {
            Vector3 vectorToNextWaypoint = path[1].cachedTransform.position - path[0].cachedTransform.position;
            _memory.orientation = Quaternion.LookRotation(Vector3.RotateTowards(_memory.cachedComponents.transform.forward, vectorToNextWaypoint, 3.1f, 0)).eulerAngles;
            //ai.Kinematic.UpdateTransformData(0);
        }
    }

    public override void Stop(AI ai)
    {
        base.Stop(ai);
    }
}