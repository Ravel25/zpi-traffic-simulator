using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINDecision]
public class SwitchingLaneAdvanceToWaitDecision : RAINDecision
{
    private VehicleMemory _memory;
    private int _lastRunning = 0;
    private float _waypointMinAcceptanceDistanceSqr;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
        VehicleMemoryAIExtension memoryExtension = ai.GetCustomElement<VehicleMemoryAIExtension>();
        _memory = memoryExtension.memory;
        _lastRunning = 0;
        _memory.arrivingDistance = (_memory.path[_memory.laneChangeAwaitingWaypointIndex].cachedTransform.position - _memory.cachedComponents.transform.position).magnitude;
        _memory.timeToChangeLane = ( -_memory.speed + Mathf.Sqrt(_memory.speed * _memory.speed + 2 * _memory.maxAcceleration * (_memory.arrivingDistance + 3)) ) / _memory.maxAcceleration;
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        if (_memory.laneChangeAwaitingWaypointIndex == _memory._currentWaypointIndex && (_memory.speed == 0 && _memory.arrivingDistance < 0.5f))
        {
            _memory.arrivingDistance = 0;
            return ActionResult.FAILURE;
        }

        ActionResult tResult = ActionResult.SUCCESS;

        for (; _lastRunning < _children.Count; _lastRunning++)
        {
            tResult = _children[_lastRunning].Run(ai);
            if (tResult != ActionResult.SUCCESS)
                break;
        }


        return tResult;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}